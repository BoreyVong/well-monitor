package open.org.kh.wellmonitor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.johnpersano.supertoasts.SuperToast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.odk.collect.wellmonitor.activities.FileManagerTabs;
import org.odk.collect.wellmonitor.activities.FormDownloadList;
import org.odk.collect.wellmonitor.activities.FormEntryActivity;
import org.odk.collect.wellmonitor.activities.GeoPointActivity;
import org.odk.collect.wellmonitor.activities.GeoPointMapActivity;
import org.odk.collect.wellmonitor.activities.InstanceChooserList;
import org.odk.collect.wellmonitor.activities.InstanceUploaderList;
import org.odk.collect.wellmonitor.activities.MyGeoPointActivity;
import org.odk.collect.wellmonitor.application.Collect;
import org.odk.collect.wellmonitor.model.Commune;
import org.odk.collect.wellmonitor.model.District;
import org.odk.collect.wellmonitor.model.Province;
import org.odk.collect.wellmonitor.tasks.DownloadFormListTask;
import org.odk.collect.wellmonitor.tasks.InstanceUploaderTask;
import org.odk.collect.wellmonitor.utilities.PropertiesUtils;
import org.opendatakit.httpclientandroidlib.client.cache.Resource;

import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import data.bridge.Column;
import data.bridge.ODKBridge;
import data.bridge.ODKDataBridge;
import data.bridge.ODKFirstOnly;


public class MainActivity extends Activity {

    CreateDB controller = new CreateDB(this);
    ListView lst_location;
    ProgressDialog pd;
    Context context;
    EditText etUsername;
    EditText etPassword;
    SharedPreferences shf;
    //Button to new form
    Button btnNewForm;
    //Button find distance below 50m
    Button btnFifty;
    //Button find distance between 50m to 100m
    Button btnHundred;
    //Button find distance more 100m to 500 m
    Button btnFiveHundred;
    //Button GPS refresh get all Data
    Button btnGPS;
    Intent i;
    InstanceUploaderTask mInstanceUploaderTask;
    AlertDialog mAlertDialog;
    private DownloadFormListTask mDownloadFormListTask;
    private static final String FORMDETAIL_KEY = "formdetailkey";
    //ListView for viewing well;
    ListView lst;
    //ArrayAdapter of display listview
    MyAdapter myArrayAdapter;
    Spinner spinnerProvince;
    Spinner spinnerDistrict;
    Spinner spinnerVillage;
    Spinner spinnerCommune;
    ODKDataBridge odkDataBridge;

    public static final String MYLOCATION_RESULT = "MYLOCATION_RESULT";
    public static final String ACCURACY_THRESHOLD = "accuracyThreshold";
    public static final String READ_ONLY = "readOnly";
    private double mAccuracyThreshold;
    public static final double DEFAULT_LOCATION_ACCURACY = 2.0;
    private static String myGPS;

    public synchronized static String getMyGPS() {
        return myGPS;
    }

    public synchronized static void setMyGPS(String myGPS) {
        MainActivity.myGPS = myGPS;
    }

    public static Context contextST;
    //get commune code to sync data from that commune
    int commId;
    ArrayList<WellProperties> arrayList = null;
    GPSTracker gpsTracker;
    private Location mLocation;
    Double lat,lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gpsTracker = new GPSTracker(getApplicationContext());
        mAccuracyThreshold = DEFAULT_LOCATION_ACCURACY;
        //Set location of Cambodia as a default
        lat = 12.565679000000000000;
        lng = 104.990962999999970000;
        displayListView(lat,lng,0, 1000000000);
        myArrayAdapter.notifyDataSetChanged();
        odkDataBridge = new ODKDataBridge();
        contextST = getApplicationContext();
        // PropertiesUtils.init(getApplicationContext());
        // must be at the beginning of any activity that can be called from an external intent
        try {
            Collect.createODKDirs();
        } catch (RuntimeException e) {
            //createErrorDialog(e.getMessage(), EXIT);
            return;
        }

        btnGPS = (Button) findViewById(R.id.btnGPS);
        btnGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {

                if(button.isPressed()){
                    btnFifty.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                    btnHundred.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                    btnFiveHundred.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                }
               // popupLocationSetting();

                PropertiesUtils.setLocationGPS(null);
                Intent i = null;
                  i = new Intent(MainActivity.this, MyGeoPointActivity.class);


                //String s = mStringAnswer.getText().toString();

                i.putExtra(READ_ONLY, true);
                i.putExtra(ACCURACY_THRESHOLD, mAccuracyThreshold);
                startActivityForResult(i, 103);

               // Toast.makeText(getApplicationContext(),getMyGPS() + " GGG ",Toast.LENGTH_LONG).show();
            }
        });

        btnNewForm = (Button) findViewById(R.id.btnNewForm);
        btnNewForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ODKDataBridge odkDataBridge = new ODKDataBridge();
                new ODKBridge(getApplicationContext()).loadFormByID("wellassessment");
                Toast.makeText(getApplicationContext(), "Test", Toast.LENGTH_SHORT).show();

            }
        });
        btnFifty = (Button) findViewById(R.id.btnFifty);
        btnFifty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                button.setSelected(!button.isSelected());
                if(button.isPressed()){
                    button.setBackgroundColor(getResources().getColor(R.color.light_orange));
                    btnHundred.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                    btnFiveHundred.setBackgroundColor(getResources().getColor(R.color.purple_dark));

                }
                else{
                    button.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                }

                displayListView(lat,lng,0, 50);
                myArrayAdapter.notifyDataSetChanged();
            }
        });

        btnHundred = (Button) findViewById(R.id.btnHundred);
        btnHundred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {

                button.setSelected(!button.isSelected());
                if(button.isPressed() || button.isFocused()){
                    button.setBackgroundColor(getResources().getColor(R.color.light_orange));
                    btnFifty.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                    btnFiveHundred.setBackgroundColor(getResources().getColor(R.color.purple_dark));

                }
                else{
                    button.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                }

                //popupLocationSetting();

                displayListView(lat,lng,50, 100);
                myArrayAdapter.notifyDataSetChanged();
            }
        });

        btnFiveHundred = (Button) findViewById(R.id.btnFiveHundred);
        btnFiveHundred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {

                button.setSelected(!button.isSelected());
                if(button.isSelected()){
                    button.setBackgroundColor(getResources().getColor(R.color.light_orange));
                    btnFifty.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                    btnHundred.setBackgroundColor(getResources().getColor(R.color.purple_dark));

                }
                else{
                    button.setBackgroundColor(getResources().getColor(R.color.purple_dark));
                }

                displayListView(lat,lng,100, 500);
                myArrayAdapter.notifyDataSetChanged();
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        popupLocationSetting();
        if(requestCode == 103){
            if(resultCode == RESULT_OK){
                String  s = intent.getStringExtra(MYLOCATION_RESULT);
                if (s.length() != 0) {
                    String[] sa = s.split(" ");
                    double gp[] = new double[4];
                    gp[0] = Double.valueOf(sa[0]).doubleValue();
                    gp[1] = Double.valueOf(sa[1]).doubleValue();
                    gp[2] = Double.valueOf(sa[2]).doubleValue();
                    gp[3] = Double.valueOf(sa[3]).doubleValue();
                    String text = "GPS - \n"+ "Lat: "+gp[0]+"\n"+"Lng: "+gp[1]+"\n" +"Acc: "+gp[2]+"\n"+"Accuracy: "+gp[3];
                   // btnGPS.setText();
                    lat = gp[0];lng = gp[1];
                    Toast.makeText(getApplicationContext(),text,Toast.LENGTH_LONG).show();
                   // setMyGPS(s + " MY GPS ");
            }
        }

        }
    }
    public void popupLocationSetting(){
        LocationManager lm = null;
        boolean gps_enabled = false,network_enabled = false;
        if(lm==null)
            lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){}
        try{
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){}

        if(!gps_enabled && !network_enabled){
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void displayListView(Double lat, Double lng,int firstDis, int secondDis) {

        ArrayList arrayListHeader = new ArrayList<HashMap<String, String>>();
        HashMap header = new HashMap<String, String>();

        header.put("id", "ID");
        header.put("village", "Village");
        header.put("owner", "Owner");
        header.put("type", "Type");
        arrayListHeader.add(header);

        SimpleAdapter simpleAdapter = new SimpleAdapter(this, arrayListHeader, R.layout.listview_layout_header,
                new String[]{"id", "village", "owner", "type"}, new int[]{R.id.txtNumber1, R.id.txtAdd1, R.id.txtOwner1, R.id.txtType1});
        ListView lstHeader = (ListView) findViewById(R.id.listHeader);
        lstHeader.setAdapter(simpleAdapter);

        arrayList = controller.getWell_location(lat,lng, firstDis, secondDis);



        //ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        myArrayAdapter = new MyAdapter(this, arrayList);

        lst = (ListView) findViewById(R.id.listWell);
        lst.setAdapter(myArrayAdapter);

        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


            }
        });


        lst.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
              /*  final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflaterHeader = getLayoutInflater();

                View header = inflaterHeader.inflate(R.layout.dialog_header, null);
                builder.setCustomTitle(header);
                TextView dialogTitle = (TextView) header.findViewById(R.id.my_custom_dialog_header);
                dialogTitle.setText("Go to Form Sections");

                LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View myView = inflater.inflate(R.layout.form_sections_monitor_dialog, null);
                builder.setView(myView);
                final AlertDialog dialog = builder.create();
                dialog.show();

                TextView infoForm = (TextView) myView.findViewById(R.id.info_form);
                TextView monitorForm = (TextView) myView.findViewById(R.id.monitor_form);
                TextView adminAndMonitorForm = (TextView) myView.findViewById(R.id.admin_monitor);
                infoForm.setText(getResources().getString(R.string.info_form));
                monitorForm.setText(getResources().getString(R.string.monitor_form));
                adminAndMonitorForm.setText(getResources().getString(R.string.admin_and_monitor));*/

                //String uri =(String) (lst.getItemAtPosition(position));
                //Toast.makeText(getApplicationContext(),uri + " URI ", Toast.LENGTH_SHORT).show();
                final TextView _uri = (TextView) view.findViewById(R.id.txtURI);
                //System.out.println(_uri.getText().toString() + " $$$$ ");
                //Toast.makeText(getApplicationContext(), _uri.getText() + " URI ", Toast.LENGTH_SHORT).show();
                /*infoForm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {*/
                //Get well Picture as string
                ArrayList<String> arrayListWellPic = controller.getWellPic(_uri.getText().toString());
                if(!arrayListWellPic.get(1).equals(null) && arrayListWellPic.get(1).length() > 0){
                    odkDataBridge.setWellPicture(new ODKFirstOnly(false, arrayListWellPic.get(1)));
                }


                ArrayList<String> arrayListData = controller.getAllRows(_uri.getText().toString());
                //System.out.println(arrayListData.get(0) + " " +arrayListData.get(1) + " *** ");
                odkDataBridge.setAddress(new ODKFirstOnly(false, arrayListData.get(1)));
                odkDataBridge.setWellOwnerName(new ODKFirstOnly(false, arrayListData.get(2)));
                odkDataBridge.setWellOwnerPhone(new ODKFirstOnly(false, arrayListData.get(3)));
                odkDataBridge.setTypeOfWell(new ODKFirstOnly(false, arrayListData.get(4)));
                odkDataBridge.setWellID(new ODKFirstOnly(false, arrayListData.get(5)));

                //GPS point, LAT, LNG, ALT, ACC
                odkDataBridge.setWellLocation(new ODKFirstOnly(false, arrayListData.get(6).trim() + " " +
                        arrayListData.get(7).trim() + " " + arrayListData.get(9).trim() + " " + arrayListData.get(8).trim()));
                        /* Skip: arrayListData.get(10) is _LAST_UPDATE_DATE, arrayListData.get(11)
                        is ADMNSTRTIVE_INFRMTON_RECORDED_DATE */
                //Set recording person
                odkDataBridge.setRecordingPerson(new ODKFirstOnly(false, arrayListData.get(12)));
                //if select recording other
                if (arrayListData.get(13).trim().equalsIgnoreCase("other_recorg") &&
                        arrayListData.get(14).length() > 0) {
                    odkDataBridge.setRecordingOrganization(new ODKFirstOnly(false, "other_recorg"));
                    odkDataBridge.setRecordingOther(new ODKFirstOnly(false, arrayListData.get(14)));
                } else {
                    //Set recording organization
                    odkDataBridge.setRecordingOrganization(new ODKFirstOnly(false, arrayListData.get(13)));
                }
                //Set well id by funding
                odkDataBridge.setWellIDByFunding(new ODKFirstOnly(false, arrayListData.get(15)));
                if (arrayListData.get(16).trim().equalsIgnoreCase("otherlocation")) {
                    //odkDataBridge.setLocationWell(new ODKFirstOnly(false, "otherlocation"));
                    odkDataBridge.setOtherLocation(new ODKFirstOnly(false, arrayListData.get(17)));
                } else {
                    //Set well location
                    odkDataBridge.setLocationWell(new ODKFirstOnly(false, arrayListData.get(16)));
                }

                if (arrayListData.get(18).trim().equalsIgnoreCase("other_funded") &&
                        arrayListData.get(19).length() > 0) {
                    //odkDataBridge.setFundedByOriginal(new ODKFirstOnly(false, "other_funded"));
                    odkDataBridge.setOtherFunded(new ODKFirstOnly(false, arrayListData.get(19)));
                } else {
                    //Funded by original
                    odkDataBridge.setFundedByOriginal(new ODKFirstOnly(false, arrayListData.get(18)));
                }
                //if select fundedbyrehabilitated is not equal [not applicable(na)]
                if (!arrayListData.get(20).trim().equalsIgnoreCase("na")) {
                    if (arrayListData.get(20).trim().equalsIgnoreCase("other_funded") &&
                            arrayListData.get(21).length() > 0) {
                        //odkDataBridge.setFundedByRehabilitated(new ODKFirstOnly(false, "other_funded"));
                        odkDataBridge.setOtherFunded1(new ODKFirstOnly(false, arrayListData.get(21)));

                    } else {
                        //Funded by fundedbyrehabilitated
                        odkDataBridge.setFundedByRehabilitated(new ODKFirstOnly(false, arrayListData.get(20)));
                    }
                    //set fundedbyrehabilitated date
                    odkDataBridge.setDateWell1(new ODKFirstOnly(false, arrayListData.get(24)));

                } else {
                    //Funded by fundedbyrehabilitated [not applicable(na)]
                    odkDataBridge.setFundedByRehabilitated(new ODKFirstOnly(false, "na"));
                }
                //Set house hole well
                odkDataBridge.setHouseholdWell(new ODKFirstOnly(false, arrayListData.get(22)));
                //Set date well
                odkDataBridge.setDateWell(new ODKFirstOnly(false, arrayListData.get(23)));
                //Primary Pump
                if (arrayListData.get(25).trim().equalsIgnoreCase("other_pump") &&
                        arrayListData.get(26).length() > 0) {
                    //odkDataBridge.setPrimaryPump(new ODKFirstOnly(false, "other_pump"));
                    //if select other pump
                    odkDataBridge.setPrimaryOther(new ODKFirstOnly(false, arrayListData.get(26)));
                } else {
                    odkDataBridge.setPrimaryPump(new ODKFirstOnly(false, arrayListData.get(25)));
                }
                //Diameter of well
                if (arrayListData.get(27).trim().equalsIgnoreCase("other_diameter") &&
                        arrayListData.get(28).length() > 0) {
                    //odkDataBridge.setDiameterOfWell(new ODKFirstOnly(false, "other_diameter"));
                    //if select other pump
                    odkDataBridge.setDiameterOther(new ODKFirstOnly(false, arrayListData.get(28)));
                } else {
                    odkDataBridge.setDiameterOfWell(new ODKFirstOnly(false, arrayListData.get(27)));
                }
                //Set Depth drill
                odkDataBridge.setDepthDrill(new ODKFirstOnly(false, arrayListData.get(29)));
                //Set how many yield of water
                odkDataBridge.setYield(new ODKFirstOnly(false, arrayListData.get(30)));
                //Set sand
                odkDataBridge.setSand(new ODKFirstOnly(false, arrayListData.get(31)));
                //Set dynamic level
                odkDataBridge.setDynamicLevel(new ODKFirstOnly(false, arrayListData.get(32)));
                //Set static level
                odkDataBridge.setStaticLevel(new ODKFirstOnly(false, arrayListData.get(33)));
                //Set used for drink
                odkDataBridge.setUsedForDrink(new ODKFirstOnly(false, arrayListData.get(34)));
                //Set used for daily life
                odkDataBridge.setUsedForDailyLife(new ODKFirstOnly(false, arrayListData.get(35)));
                odkDataBridge.setPlatform(new ODKFirstOnly(false, arrayListData.get(36)));
                odkDataBridge.setWellPlatform(new ODKFirstOnly(false, arrayListData.get(37)));
                odkDataBridge.setPlatformDesign(new ODKFirstOnly(false, arrayListData.get(38)));
                odkDataBridge.setDrainDesign(new ODKFirstOnly(false, arrayListData.get(39)));
                odkDataBridge.setFlooded(new ODKFirstOnly(false, arrayListData.get(40)));
                odkDataBridge.setHeight(new ODKFirstOnly(false, arrayListData.get(41)));
                odkDataBridge.setWellFlooded(new ODKFirstOnly(false, arrayListData.get(42)));
                odkDataBridge.setYearFlooded(new ODKFirstOnly(false, arrayListData.get(43)));
                odkDataBridge.setHighestFlooded(new ODKFirstOnly(false, arrayListData.get(44)));

                //Set well platform picture
                ArrayList<String> arrayListWellPlatformPic = controller.getWellPlatformPic(_uri.getText().toString());
                //System.out.println(" Heeheee " + arrayListWellPlatformPic.get(1));
                odkDataBridge.setPlatformPicture(new ODKFirstOnly(false, arrayListWellPlatformPic.get(1)));
                //Set WaterPoint(is the well functional usable)
                odkDataBridge.setWaterPoint(new ODKFirstOnly(false, arrayListData.get(45)));
                if (!arrayListData.get(45).trim().equalsIgnoreCase("fully_functional")) {
                    odkDataBridge.setDamaged(new ODKFirstOnly(false, arrayListData.get(46)));
                    if (arrayListData.get(46).trim().equalsIgnoreCase("other_damaged")) {
                        //Set damagedOther
                        odkDataBridge.setDamagedOther(new ODKFirstOnly(false, arrayListData.get(47)));
                    }
                }
                //Set Hardware damaged
                ArrayList<String> arrayListHardware = controller.getHardwareDamaged(_uri.getText().toString());
                String hardware = "";
                //ArrayList<String>arrayListHardware = controller.getHardwareDamaged("uuid:e011511f-32d4-4848-846d-ee61a1e24eb4");
                //arrayListHardware.add(3,"other_hardware");
                for (int m = 0; m < arrayListHardware.size(); m++) {
                    if (arrayListHardware.get(m).equalsIgnoreCase("other_hardware")) {
                        odkDataBridge.setOtherDamaged(new ODKFirstOnly(false, arrayListData.get(48)));
                    }
                    hardware += arrayListHardware.get(m) + " ";

                }
                odkDataBridge.setHardwareDamaged(new ODKFirstOnly(false, hardware));

                //Set Environment Condition
                odkDataBridge.setEnvironmentCon(new ODKFirstOnly(false, arrayListData.get(49)));
                //odkDataBridge.setEnvironmentCon(new ODKFirstOnly(false, "not_clean"));
                //Set type of Dirt
                String typeDirt = "";
                ArrayList<String> arrayListTypeDirt = controller.getTypeDirt(_uri.getText().toString());
                //ArrayList<String> arrayListTypeDirt = controller.getTypeDirt("uuid:eca86f8d-bcd5-493f-b552-0f113a66af44");
                //arrayListTypeDirt.add(2,"other");
                for (int n = 0; n < arrayListTypeDirt.size(); n++) {
                    if (arrayListTypeDirt.get(n).equalsIgnoreCase("other")) {
                        //Set type dirt other
                        odkDataBridge.setTypeDirtOther(new ODKFirstOnly(false, arrayListData.get(50)));
                        //odkDataBridge.setTypeDirtOther(new ODKFirstOnly(false, "Other"));
                    }
                    typeDirt += arrayListTypeDirt.get(n) + " ";
                }
                odkDataBridge.setTypeDirt(new ODKFirstOnly(false, typeDirt));


                //Set Quantity water
                odkDataBridge.setQuantityWater(new ODKFirstOnly(false, arrayListData.get(51)));
                //odkDataBridge.setQuantityWater(new ODKFirstOnly(false, "dry"));
                //Set supply seasonal
                odkDataBridge.setSupplySeasonal(new ODKFirstOnly(false, arrayListData.get(52)));
                //Set water taste
                odkDataBridge.setTaste(new ODKFirstOnly(false, arrayListData.get(53)));
                //odkDataBridge.setTaste(new ODKFirstOnly(false, "not_acceptable"));
                //Set water taste1
                ArrayList<String> arrayListTaste1 = controller.getTaste1(_uri.getText().toString());
                //ArrayList<String> arrayListTaste1 = controller.getTaste1("uuid:4bec929c-e817-4d45-9d48-a98f73bf49d9");
                String taste1 = "";
                for (int o = 0; o < arrayListTaste1.size(); o++) {
                    if (arrayListTaste1.get(o).equalsIgnoreCase("other_taste")) {
                        //Set other taste
                        odkDataBridge.setOtherTaste(new ODKFirstOnly(false, arrayListData.get(54)));
                        //odkDataBridge.setTypeDirtOther(new ODKFirstOnly(false, "Other"));
                    }
                    taste1 += arrayListTaste1.get(o) + " ";
                }
                odkDataBridge.setTaste1(new ODKFirstOnly(false, taste1));

                //Set waterqual monitoring
                odkDataBridge.setWaterQualMonitoring(new ODKFirstOnly(false, arrayListData.get(55)));
                if (arrayListData.get(55).length() > 0 && arrayListData.get(55).equalsIgnoreCase("yes")) {
                    odkDataBridge.setArsenic(new ODKFirstOnly(false, arrayListData.get(56)));
                    odkDataBridge.setPh(new ODKFirstOnly(false, arrayListData.get(57)));
                    odkDataBridge.setFreeChlorine(new ODKFirstOnly(false, arrayListData.get(58)));
                    odkDataBridge.setNitrate(new ODKFirstOnly(false, arrayListData.get(59)));
                    odkDataBridge.setNitrite(new ODKFirstOnly(false, arrayListData.get(60)));
                    odkDataBridge.setIron(new ODKFirstOnly(false, arrayListData.get(61)));
                    odkDataBridge.setHardness(new ODKFirstOnly(false, arrayListData.get(62)));
                    odkDataBridge.setTurbidity(new ODKFirstOnly(false, arrayListData.get(63)));

                    //set e_coli, flouride, manganese, tds, tasteOdour
                    odkDataBridge.setNitrite(new ODKFirstOnly(false, arrayListData.get(67)));
                    odkDataBridge.setIron(new ODKFirstOnly(false, arrayListData.get(68)));
                    odkDataBridge.setHardness(new ODKFirstOnly(false, arrayListData.get(69)));
                    odkDataBridge.setTurbidity(new ODKFirstOnly(false, arrayListData.get(70)));
                    odkDataBridge.setInform(new ODKFirstOnly(false, arrayListData.get(71)));

                }
                //Set Inform
                odkDataBridge.setInform(new ODKFirstOnly(false, arrayListData.get(64)));
                        /* Set udpated new field */
                //set household well1
                odkDataBridge.setHouseholdWell1(new ODKFirstOnly(false, arrayListData.get(65)));
                //Set number of afffected family
                odkDataBridge.setNumberFamilyAffected(new ODKFirstOnly(false, arrayListData.get(66)));

                //Set Hidden to mark monitoring the well
                odkDataBridge.setHidden(new ODKFirstOnly(false, "Active"));


                SharedPreferences myPrf = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = myPrf.edit();
                editor.putBoolean("FormInfo", true);
                editor.commit();

                new ODKBridge(getApplicationContext()).loadFormByID("wellassessment", odkDataBridge);

                return false;
            }
        });

    }
    /* Menu item selected event */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        Intent i;
        switch (item.getItemId()) {
            //case select sync data
            case R.id.action_syn:
                if (isNetworkConnected()) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    LayoutInflater inflaterHeader = getLayoutInflater();

                    View header = inflaterHeader.inflate(R.layout.dialog_header, null);
                    builder.setCustomTitle(header);
                    TextView dialogTitle = (TextView) header.findViewById(R.id.my_custom_dialog_header);
                    dialogTitle.setText("Select Address...");

                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View myView = inflater.inflate(R.layout.address_layout_sync, null);
                    builder.setView(myView);
                    final AlertDialog dialog = builder.create();
                    dialog.show();
                    spinnerProvince = (Spinner) dialog.findViewById(R.id.address_province);
                    spinnerDistrict = (Spinner) dialog.findViewById(R.id.address_district);
                    spinnerCommune = (Spinner) dialog.findViewById(R.id.address_commune);

                    //Add province key(province Name Kh) and value(Province code) to hashmap
                    final HashMap<String, String> proMap = new HashMap<String, String>();
                    //Add district key(district name kh) and value(District code) to hashmap
                    final HashMap<String, String> distMap = new HashMap<String, String>();
                    //Add communes key(commune name kh) and value(Commune code) to hasmap
                    final HashMap<String, String> commMap = new HashMap<String, String>();

                    final String lblChoose = getApplicationContext().getResources().getString(R.string.lblChoose);
                    List<Province> provinceList = new Province(getApplicationContext()).getListProvinces();
                    String[] spinnerArrayPro = new String[provinceList.size()];
                    spinnerArrayPro[0] = lblChoose;
                    for (int j = 0; j < provinceList.size(); j++) {
                        proMap.put(provinceList.get(j).getProvinceKh(), String.valueOf(provinceList.get(j).getProCode()));
                        spinnerArrayPro[j] = provinceList.get(j).getProvinceKh();
                    }

                    ArrayAdapter<String> arrayAdapterPro = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_list, spinnerArrayPro);
                    arrayAdapterPro.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerProvince.setAdapter(arrayAdapterPro);

                    //set on item selected province
                    spinnerProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String proName = spinnerProvince.getSelectedItem().toString();
                            List<District> districtList = null;
                            int proId = Integer.parseInt(proMap.get(proName));

                            try {
                                districtList = new District(getApplicationContext()).getByProvinceId(proId);
                                String[] spinnerArrayDist = new String[districtList.size()];

                                for (int k = 0; k < districtList.size(); k++) {
                                    distMap.put(districtList.get(k).getdNameKh(), String.valueOf(districtList.get(k).getdCode()));
                                    spinnerArrayDist[k] = districtList.get(k).getdNameKh();
                                }
                                ArrayAdapter<String> arrayAdapterDist = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_list, spinnerArrayDist);
                                arrayAdapterDist.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinnerDistrict.setAdapter(arrayAdapterDist);

                            } catch (ParseException e) {
                                System.out.println(e + " Can't parse province code to Integer");
                            }

                            //Toast.makeText(getApplicationContext(),proId+"  ",Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    //set on item selected district
                    spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String distName = spinnerDistrict.getSelectedItem().toString();
                            int distId = Integer.parseInt(distMap.get(distName));
                            List<Commune> communesList = null;
                            try {
                                communesList = new Commune(getApplicationContext()).getByDistrictId(distId);
                                String[] spinnerArrayComm = new String[communesList.size()];

                                for (int l = 0; l < communesList.size(); l++) {
                                    commMap.put(communesList.get(l).getcNameKh(), String.valueOf(communesList.get(l).getcCode()));
                                    spinnerArrayComm[l] = communesList.get(l).getcNameKh();
                                }
                                ArrayAdapter<String> arrayAdapterComm = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_list, spinnerArrayComm);
                                arrayAdapterComm.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinnerCommune.setAdapter(arrayAdapterComm);

                            } catch (ParseException e) {
                                System.out.println(e + " Can't parse district code to Integer");
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    //set on item selected commune
                    spinnerCommune.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String commName = spinnerCommune.getSelectedItem().toString();
                            commId = Integer.parseInt(commMap.get(commName));
                            //Toast.makeText(getApplicationContext(),commId+"  ",Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    Button btnOk = (Button) dialog.findViewById(R.id.btnOkSync);
                    Button btnCancel = (Button) dialog.findViewById(R.id.btnCancelSync);
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            syncSqliteData();
                            dialog.hide();

                        }
                    });
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });


                }
                break;
            // case download blank form
            case R.id.action_blankForm:
                if (isNetworkConnected()) {
                    Collect.getInstance().getActivityLogger().logAction(this, "downloadBlankForms", "click");
                    i = new Intent(getApplicationContext(), FormDownloadList.class);
                    startActivity(i);
                }
                break;
            // case edit saved form
            case R.id.action_editForm:
                if (isNetworkConnected()) {
                    Collect.getInstance().getActivityLogger()
                            .logAction(this, "editSavedForm", "click");
                    i = new Intent(getApplicationContext(),
                            InstanceChooserList.class);
                    startActivity(i);
                }
                break;
            // case send finalized monitor forms
            case R.id.action_send_monitor_forms:
                if (isNetworkConnected()) {
                    Collect.getInstance().getActivityLogger()
                            .logAction(this, "uploadForms", "click");
                   i = new Intent(getApplicationContext(),
                            InstanceUploaderList.class);
                    startActivity(i);
                }
                break;
            // case delete monitor forms
            case R.id.action_delete_forms:
                if (isNetworkConnected()) {
                    Collect.getInstance().getActivityLogger().logAction(this, "deleteSavedForms", "click");
                    i = new Intent(getApplicationContext(), FileManagerTabs.class);
                    startActivity(i);
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean myLogin(String username, String password) {

        return true;
    }

    public void syncSqliteData() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("jsonPost", commId + "_" + controller.selectMaxDate());
        client.post("http://aggregate.open.org.kh/download_well_location.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {

                try {
                    JSONArray arr = new JSONArray(response);
                    for (int i = 0; i < arr.length(); i++) {
                        System.out.println(" Length " + arr.length());
                        JSONObject jObj = (JSONObject) arr.get(i);
                        Column.ADDRESS = jObj.getString("ADMNSTRTIVE_INFRMTON_ADDRESS");
                        Column.WELLOWNERNAME = jObj.getString("INFO_ABOUT_WELL_WELL_OWNER_NAME");
                        Column.TYPEOFWELL = jObj.getString("INFO_ABOUT_WELL_TYPEOFWELL");
                        Column.WELLOWNERPHONE = jObj.getString("INFO_ABOUT_WELL_WELLOWNER_PHONE");
                        Column.WELLID = jObj.getString("INFO_ABOUT_WELL_WELL_ID");
                        Column.LOCATIONLAT = jObj.getString("INFO_ABOUT_WELL_WELL_LOCATION_LAT");
                        Column.LOCATIONLNG = jObj.getString("INFO_ABOUT_WELL_WELL_LOCATION_LNG");
                        Column.LOCATIONACC = jObj.getString("INFO_ABOUT_WELL_WELL_LOCATION_ACC");
                        Column.LOCATIONALT = jObj.getString("INFO_ABOUT_WELL_WELL_LOCATION_ALT");
                        Column.LASTUPDATEDATE = jObj.getString("_LAST_UPDATE_DATE");
                        Column.RECORDEDDATE = jObj.getString("ADMNSTRTIVE_INFRMTON_RECORDED_DATE");
                        Column.RECORDINGPERSON = jObj.getString("ADMNSTRTIVE_INFRMTON_RECORDING_PERSON");

                        Column.RECORDINGORGANIZATION = jObj.getString("ADMNSTRTIVE_INFRMTON_RECORDING_ORGANIZATION");
                        Column.RECORDINGOTHER = jObj.getString("ADMNSTRTIVE_INFRMTON_RECORDING_OTHER");
                        Column.WELLIDBYFUNDING = jObj.getString("INFO_ABOUT_WELL_WELL_ID_BY_FUNDING");
                        Column.LOCATIONWELL = jObj.getString("INFO_ABOUT_WELL_LOCATION_WELL");
                        Column.OTHERLOCATION = jObj.getString("INFO_ABOUT_WELL_OTHERLOCATION");
                        Column.FUNDEDBYORIGINAL = jObj.getString("INFO_ABOUT_WELL_FUNDEDBYORIGINAL");
                        Column.OTHERFUND = jObj.getString("INFO_ABOUT_WELL_OTHERFUNDED");
                        Column.FUNDEDBYREHABILITATED = jObj.getString("INFO_ABOUT_WELL_FUNDEDBYREHABILITATED");
                        Column.OTHERFUNDED1 = jObj.getString("INFO_ABOUT_WELL_OTHERFUNDED1");
                        Column.HOUSEHOLDWELL = jObj.getString("INFO_ABOUT_WELL_HOUSEHOLD_WELL");

                        Column.DATEWELL = jObj.getString("INFO_ABOUT_WELL_DATE_WELL");
                        Column.DATEWELL1 = jObj.getString("INFO_ABOUT_WELL_DATE_WELL1");
                        Column.PRIMARYPUMP = jObj.getString("INFO_ABOUT_WELL_PRIMARYPUMP");
                        Column.PRIMARYOTHER = jObj.getString("INFO_ABOUT_WELL_PRIMARYOTHER");
                        Column.DIAMETEROFWELL = jObj.getString("INFO_ABOUT_WELL_DIAMETER_OF_WELL");
                        Column.DIAMETEROTHER = jObj.getString("INFO_ABOUT_WELL_DIAMETER_OTHER");
                        Column.DEPTHDRILL = jObj.getString("INFO_ABOUT_WELL_DEPTHDRILL");
                        Column.YIELD = jObj.getString("INFO_ABOUT_WELL_YIELD");
                        Column.SAND = jObj.getString("INFO_ABOUT_WELL_SAND");
                        Column.DYNAMICLEVEL = jObj.getString("INFO_ABOUT_WELL_DYNAMIC");

                        Column.STATICLEVEL = jObj.getString("INFO_ABOUT_WELL_STATIC");
                        Column.USEDFORDRINK = jObj.getString("INFO_ABOUT_WELL_USEDFORDRINK");
                        Column.USEDFORDAILYLIFE = jObj.getString("INFO_ABOUT_WELL_USEDFORDAILYLIFE");
                        Column.PLATFORM = jObj.getString("INFO_ABOUT_WELL_PLATFORM");
                        Column.WELLPLATFORM = jObj.getString("INFO_ABOUT_WELL_WELLPLATFORM");
                        Column.PLATFORMDESIGN = jObj.getString("INFO_ABOUT_WELL_PLATFORMDESIGN");
                        Column.DRAINDESIGN = jObj.getString("INFO_ABOUT_WELL_DRAINDESIGN");
                        Column.FLOODED = jObj.getString("INFO_ABOUT_WELL_FLOODED");
                        Column.HEIGHT = jObj.getString("INFO_ABOUT_WELL_HEIGHT");
                        Column.WELLFLOODED = jObj.getString("INFO_ABOUT_WELL_WELLFLOODED");

                        Column.YEARFLOODED = jObj.getString("INFO_ABOUT_WELL_YEARFLOODED");
                        //Monitoring
                        Column.HIGHESTFLOODED = jObj.getString("INFO_ABOUT_WELL_HIGHESTFLOODED");
                        Column.WATERPOINT = jObj.getString("MONTORING_INFORMATON_WATERPOINT");
                        Column.DAMAGED = jObj.getString("MONTORING_INFORMATON_DAMAGED");
                        Column.DAMAGEDOTHER = jObj.getString("MONTORING_INFORMATON_DAMAGEDOTHER");
                        Column.OTHERDAMAGED = jObj.getString("MONTORING_INFORMATON_OTHERDAMAGED");
                        Column.ENVIRONMENTCON = jObj.getString("MONTORING_INFORMATON_ENVIRONMENTCON");

                        Column.TYPEDIRTOTHER = jObj.getString("MONTORING_INFORMATON_TYPEDIRT_OTHER");
                        Column.QUANTITYWATER = jObj.getString("MONTORING_INFORMATON_QUANTITYWATER");
                        Column.SUPPLYSEASONAL = jObj.getString("MONTORING_INFORMATON_SUPPLYSEASONAL");
                        Column.TASTE = jObj.getString("MONTORING_INFORMATON_TASTE");
                        Column.OTHERTASTE = jObj.getString("MONTORING_INFORMATON_OTHERTASTE");
                        Column.WATERQUALMONITORING = jObj.getString("MONTRNG_NFRMTN_DTIL_WATERQUAL_MONITORING");
                        Column.ARSENIC = jObj.getString("MONTRNG_NFRMTN_DTIL_ARSENIC");
                        Column.PH = jObj.getString("MONTRNG_NFRMTN_DTIL_PH");
                        Column.FREECLORINE = jObj.getString("MONTRNG_NFRMTN_DTIL_FREE_CHLORINE");
                        Column.NITRATE = jObj.getString("MONTRNG_NFRMTN_DTIL_NITRATE");

                        Column.NITRITE = jObj.getString("MONTRNG_NFRMTN_DTIL_NITRITE");
                        Column.IRON = jObj.getString("MONTRNG_NFRMTN_DTIL_IRON");
                        Column.HARDNESS = jObj.getString("MONTRNG_NFRMTN_DTIL_HARDNESS");
                        Column.TUBIDITY = jObj.getString("MONTRNG_NFRMTN_DTIL_TURBIDITY");
                        Column.E_COLI = jObj.getString("MONTRNG_NFRMTN_DTIL_E_COLI");

                        Column.FLOURIDE = jObj.getString("MONTRNG_NFRMTN_DTIL_FLOURIDE");
                        Column.MANGANESE = jObj.getString("MONTRNG_NFRMTN_DTIL_MANGANESE");
                        Column.TDS = jObj.getString("MONTRNG_NFRMTN_DTIL_TDS");
                        Column.TASTEODOUR = jObj.getString("MONTRNG_NFRMTN_DTIL_TASTE_ODOUR");

                        Column.INFORM = jObj.getString("MONTRNG_NFRMTN_DTIL_INFORM");
                        Column.TUBIDITY = jObj.getString("INFO_ABOUT_WELL_HOUSEHOLD_WELL1");
                        Column.AFFECTEDFAMILY = jObj.getString("INFO_ABOUT_WELL_AFFECTED_FAMILY");

                        Column.HIDDEN = jObj.getString("MONTRNG_NFRMTN_DTIL_HIDDEN");
                        Column._URI = jObj.getString("_URI");


                        //Insert data to Well Assessment core
                        controller.insertRow(
                                Column.ADDRESS, Column.WELLOWNERNAME, Column.TYPEOFWELL, Column.WELLOWNERPHONE, Column.WELLID,
                                Column.LOCATIONLAT, Column.LOCATIONLNG, Column.LOCATIONACC, Column.LOCATIONALT, Column.LASTUPDATEDATE, Column.RECORDEDDATE, Column.RECORDINGPERSON,
                                Column.RECORDINGORGANIZATION, Column.RECORDINGOTHER, Column.WELLIDBYFUNDING, Column.LOCATIONWELL, Column.OTHERLOCATION,
                                Column.FUNDEDBYORIGINAL, Column.OTHERFUND, Column.FUNDEDBYREHABILITATED, Column.OTHERFUNDED1, Column.HOUSEHOLDWELL,
                                Column.DATEWELL, Column.DATEWELL1, Column.PRIMARYPUMP, Column.PRIMARYOTHER, Column.DIAMETEROFWELL,
                                Column.DIAMETEROTHER, Column.DEPTHDRILL, Column.YIELD, Column.SAND, Column.DYNAMICLEVEL,
                                Column.STATICLEVEL, Column.USEDFORDRINK, Column.USEDFORDAILYLIFE, Column.PLATFORM, Column.WELLPLATFORM,
                                Column.PLATFORMDESIGN, Column.DRAINDESIGN, Column.FLOODED, Column.HEIGHT, Column.WELLFLOODED,
                                Column.YEARFLOODED, Column.HIGHESTFLOODED, Column.WATERPOINT, Column.DAMAGED, Column.DAMAGEDOTHER,
                                Column.OTHERDAMAGED, Column.ENVIRONMENTCON, Column.TYPEDIRTOTHER, Column.QUANTITYWATER,
                                Column.SUPPLYSEASONAL, Column.TASTE, Column.OTHERTASTE, Column.WATERQUALMONITORING,
                                Column.ARSENIC, Column.PH, Column.FREECLORINE, Column.NITRATE,
                                Column.NITRITE, Column.IRON, Column.HARDNESS, Column.TUBIDITY,
                                Column.INFORM, Column.HOUSEHOLDWELL1, Column.AFFECTEDFAMILY,
                                Column.E_COLI, Column.FLOURIDE, Column.MANGANESE,
                                Column.TDS, Column.TASTEODOUR,
                                Column.HIDDEN, Column._URI);

                        //Get json object and insert data to well platform pictures
                        String subResponsePic = jObj.getString("0");
                        JSONArray platJsonArray = new JSONArray(subResponsePic);
                        for (int j = 0; j < platJsonArray.length(); j++) {
                            JSONObject jObjPlat = (JSONObject) platJsonArray.get(j);
                            Column.PLATFORMPICTURE = jObjPlat.getString("blobvalue");
                            Column._TOP_LEVEL_AURI_PLAT = jObjPlat.getString("_TOP_LEVEL_AURI_PLAT");
                            try{
                                controller.insertPlatformPic(Column._TOP_LEVEL_AURI_PLAT, Column.PLATFORMPICTURE);
                            }
                            catch(Exception e){
                                controller.insertPlatformPic(Column._TOP_LEVEL_AURI_PLAT, Column.PLATFORMPICTURE);
                            }

                        }
                        //Get json object and insert data to well pictures
                        JSONArray picJsonArray = new JSONArray(jObj.getString("1"));
                        for (int h = 0; h < picJsonArray.length(); h++) {
                            JSONObject jObjPic = (JSONObject) picJsonArray.get(h);
                            Column.PICTURE = jObjPic.getString("blobwellval");
                            Column._TOP_LEVEL_AURI_PIC = jObjPic.getString("_TOP_LEVEL_AURI_PIC");
                            controller.insertPic(Column._TOP_LEVEL_AURI_PIC, Column.PICTURE);

                        }

                        //Get json object and insert data into hardware damaged
                        JSONArray hardJsonArray = new JSONArray(jObj.getString("2"));
                        for (int k = 0; k < hardJsonArray.length(); k++) {
                            JSONObject hObj = (JSONObject) hardJsonArray.get(k);
                            Column._H_URI = hObj.getString("_H_URI");
                            Column._H_PARENT_AURI = hObj.getString("_H_PARENT_AURI");
                            Column._H_VALUE = hObj.getString("_H_VALUE");
                            controller.insertHardwareDamaged(Column._H_URI, Column._H_PARENT_AURI, Column._H_VALUE);
                        }

                        //Get json object and insert data into type dirt
                        JSONArray typeDirtJsonArray = new JSONArray(jObj.getString("3"));
                        for (int k = 0; k < typeDirtJsonArray.length(); k++) {
                            JSONObject tdObj = (JSONObject) typeDirtJsonArray.get(k);
                            Column._TD_URI = tdObj.getString("_TD_URI");
                            Column._TD_PARENT_AURI = tdObj.getString("_TD_PARENT_AURI");
                            Column._TD_VALUE = tdObj.getString("_TD_VALUE");
                            controller.insertTypeDirt(Column._TD_URI, Column._TD_PARENT_AURI, Column._TD_VALUE);
                        }

                        //Get json object and insert data into taste1
                        JSONArray taste1JsonArray = new JSONArray(jObj.getString("4"));
                        for (int k = 0; k < taste1JsonArray.length(); k++) {
                            JSONObject taObj = (JSONObject) taste1JsonArray.get(k);
                            Column._TA_URI = taObj.getString("_TA_URI");
                            Column._TA_PARENT_AURI = taObj.getString("_TA_PARENT_AURI");
                            Column._TA_VALUE = taObj.getString("_TA_VALUE");
                            controller.insertTaste1(Column._TA_URI, Column._TA_PARENT_AURI, Column._TA_VALUE);
                        }

                        Toast.makeText(getApplicationContext(), controller.dbSyncCountPlatformPic() + " platform Count ", Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getApplicationContext(),commId+" Count",Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getApplicationContext(),controller.dbSyncCount()+" Count",Toast.LENGTH_SHORT).show();
                        System.out.println(" Sync Complete #######  ");

                    }
                } catch (Exception e) {
                    System.out.print(e + " decode error.");
                }

                //Toast.makeText(getApplicationContext(), "DB Sync completed! " + response, Toast.LENGTH_LONG).show();

                displayListView(lat,lng,0, 1000000000);
                myArrayAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(int statusCode, Throwable error,
                                  String content) {
                // TODO Auto-generated method stub
                //pd.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), error + " Check internet connection again!", Toast.LENGTH_LONG).show();
                }
            }

        });


    }

    public void syncSqliteUser() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("jsonUserId", controller.selectMaxUserID() + "");
        client.post("http://192.168.2.222/WellGPS/user.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONArray arr = new JSONArray(response);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject jObj = (JSONObject) arr.get(i);
                        String id = jObj.getString("id");
                        String username = jObj.getString("username");
                        String password = jObj.getString("password");
                        String org = jObj.getString("organization");
                        controller.insertUser(id, username, password, org);
                    }

                } catch (Exception e) {
                    System.out.print(e + " decode error.");
                }

            }

            @Override
            public void onFailure(int statusCode, Throwable error,
                                  String content) {
                // TODO Auto-generated method stub
                //pd.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                } else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Check internet connection again!", Toast.LENGTH_LONG).show();
                }
            }

        });


    }

    public boolean isNetworkConnected() {
        ConnectivityManager cn = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni = cn.getActiveNetworkInfo();
        if (ni != null && ni.isConnectedOrConnecting()) {
            try {
                //This is to set allow network operation in UI, which is usually in background mode
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                // Network is available but check if we can get access from the
                // network.
                URL url = new URL("http://www.google.com");
                HttpURLConnection urlc = (HttpURLConnection) url
                        .openConnection();
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(2000); // Timeout 2 seconds.
                urlc.connect();

                if (urlc.getResponseCode() == 200) // Successful response.
                {
                    return true;
                } else {
                    Log.d("NO INTERNET", "NO INTERNET");
                    Toast.makeText(getApplicationContext(), "URL down", Toast.LENGTH_SHORT).show();
                    return false;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), "No network available", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
       /*displayListView(50);
       myArrayAdapter.notifyDataSetChanged();*/
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SuperToast.cancelAllSuperToasts();
    }


}
