package open.org.kh.wellmonitor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Borey on 3/1/15.
 */
public class MyAdapter extends ArrayAdapter<WellProperties> {

    Context context;

    public MyAdapter(Context context, ArrayList<WellProperties> wellProperties){
       super(context,0,wellProperties);
        this.context = context;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Get the data item for this position
        WellProperties wellProperties = getItem(position);
        if(convertView == null){
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.listview_layout,parent,false);

        }
        TextView txtNumber = (TextView)convertView.findViewById(R.id.txtNumber);
        TextView txtAdd = (TextView)convertView.findViewById(R.id.txtAdd);
        TextView txtOwner = (TextView)convertView.findViewById(R.id.txtOwner);
        TextView txtType = (TextView)convertView.findViewById(R.id.txtType);
        TextView txtURI = (TextView)convertView.findViewById(R.id.txtURI);

        txtNumber.setText(wellProperties.getId());
        txtAdd.setText(wellProperties.getAdd());
        txtOwner.setText(wellProperties.getOwner());
        txtType.setText(wellProperties.getType());
        txtURI.setText(wellProperties.getUri());

        txtURI.setVisibility(View.GONE);


        return convertView;
    }
}
