package open.org.kh.wellmonitor;

/**
 * Created by Borey on 3/2/15.
 */
public class WellProperties{
    private String id;
    private String add;
    private String owner;
    private String type;
    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public WellProperties(){
        super();
    }
    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public WellProperties(String id, String add, String owner, String type,String uri){
        this.id = id;
        this.add = add;
        this.owner = owner;
        this.type = type;
        this.uri = uri;
    }


}
