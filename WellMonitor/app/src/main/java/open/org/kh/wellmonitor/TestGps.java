package open.org.kh.wellmonitor;

/**
 * Created by Borey on 3/16/15.
 */
public class TestGps {

    static double earthRadius = 6371000; //meters

    public static void main(String[] args){
        long radius = 6371000;
        double lat1 = 11.5398112;
        double lat2 = 11.5399463;
        double lon1 = 104.9129919;
        double lon2 = 104.9138598;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
         double c1 = 2 * Math.asin(Math.sqrt(a));
         double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        System.out.println("GPS C1 : " + ((float)c1 * earthRadius));
        System.out.println("GPS C: " + (c * earthRadius));


        System.out.println("GPS : " + (radius * c));

        System.out.println("GPS Han : "+ distFrom(11.5398112, 104.9129919, 11.5399463, 104.9138598));

    }

    public static float distFrom(double lat1, double lng1, double lat2, double lng2) {

        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }

}
