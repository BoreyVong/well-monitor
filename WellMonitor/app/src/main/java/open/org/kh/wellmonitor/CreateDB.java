package open.org.kh.wellmonitor;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;

import data.bridge.Column;

/**
 * Created by Borey on 2/24/15.
 */
public class CreateDB extends SQLiteOpenHelper {

    public CreateDB(Context context){
        super(context,"well_location.db",null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "create table if not exists WELLASSESSMENT_CORE(id INTEGER,ADMNSTRTIVE_INFRMTON_ADDRESS TEXT," +
                "INFO_ABOUT_WELL_WELL_OWNER_NAME TEXT,INFO_ABOUT_WELL_WELLOWNER_PHONE TEXT,INFO_ABOUT_WELL_TYPEOFWELL TEXT, " +
                "INFO_ABOUT_WELL_WELL_ID TEXT,INFO_ABOUT_WELL_WELL_LOCATION_LAT TEXT,INFO_ABOUT_WELL_WELL_LOCATION_LNG TEXT, " +
                "INFO_ABOUT_WELL_WELL_LOCATION_ACC TEXT, INFO_ABOUT_WELL_WELL_LOCATION_ALT TEXT, "+
                "_LAST_UPDATE_DATE TEXT, ADMNSTRTIVE_INFRMTON_RECORDED_DATE TEXT, ADMNSTRTIVE_INFRMTON_RECORDING_PERSON TEXT, " +
                "ADMNSTRTIVE_INFRMTON_RECORDING_ORGANIZATION TEXT,ADMNSTRTIVE_INFRMTON_RECORDING_OTHER TEXT, INFO_ABOUT_WELL_WELL_ID_BY_FUNDING TEXT,"+
                "INFO_ABOUT_WELL_LOCATION_WELL TEXT, INFO_ABOUT_WELL_OTHERLOCATION TEXT,INFO_ABOUT_WELL_FUNDEDBYORIGINAL TEXT," +
                "INFO_ABOUT_WELL_OTHERFUNDED TEXT,INFO_ABOUT_WELL_FUNDEDBYREHABILITATED TEXT, INFO_ABOUT_WELL_OTHERFUNDED1 TEXT," +
                "INFO_ABOUT_WELL_HOUSEHOLD_WELL TEXT,INFO_ABOUT_WELL_DATE_WELL TEXT, INFO_ABOUT_WELL_DATE_WELL1 TEXT, " +
                "INFO_ABOUT_WELL_PRIMARYPUMP TEXT, INFO_ABOUT_WELL_PRIMARYOTHER TEXT,INFO_ABOUT_WELL_DIAMETER_OF_WELL TEXT," +
                "INFO_ABOUT_WELL_DIAMETER_OTHER TEXT,INFO_ABOUT_WELL_DEPTHDRILL TEXT, INFO_ABOUT_WELL_YIELD TEXT," +
                "INFO_ABOUT_WELL_SAND TEXT, INFO_ABOUT_WELL_DYNAMIC TEXT, INFO_ABOUT_WELL_STATIC TEXT, " +
                "INFO_ABOUT_WELL_USEDFORDRINK TEXT, INFO_ABOUT_WELL_USEDFORDAILYLIFE TEXT, INFO_ABOUT_WELL_PLATFORM TEXT, " +
                "INFO_ABOUT_WELL_WELLPLATFORM TEXT,INFO_ABOUT_WELL_PLATFORMDESIGN TEXT, INFO_ABOUT_WELL_DRAINDESIGN TEXT, " +
                "INFO_ABOUT_WELL_FLOODED TEXT, INFO_ABOUT_WELL_HEIGHT TEXT, INFO_ABOUT_WELL_WELLFLOODED TEXT," +
                "INFO_ABOUT_WELL_YEARFLOODED TEXT,INFO_ABOUT_WELL_HIGHESTFLOODED TEXT, MONTORING_INFORMATON_WATERPOINT TEXT, " +
                "MONTORING_INFORMATON_DAMAGED TEXT, MONTORING_INFORMATON_DAMAGEDOTHER TEXT,MONTORING_INFORMATON_OTHERDAMAGED TEXT," +
                "MONTORING_INFORMATON_ENVIRONMENTCON TEXT, MONTORING_INFORMATON_TYPEDIRT_OTHER TEXT,MONTORING_INFORMATON_QUANTITYWATER TEXT," +
                "MONTORING_INFORMATON_SUPPLYSEASONAL TEXT, MONTORING_INFORMATON_TASTE TEXT,MONTORING_INFORMATON_OTHERTASTE TEXT," +
                "MONTRNG_NFRMTN_DTIL_WATERQUAL_MONITORING TEXT, MONTRNG_NFRMTN_DTIL_ARSENIC TEXT,MONTRNG_NFRMTN_DTIL_PH TEXT," +
                "MONTRNG_NFRMTN_DTIL_FREE_CHLORINE TEXT, MONTRNG_NFRMTN_DTIL_NITRATE TEXT,MONTRNG_NFRMTN_DTIL_NITRITE TEXT," +
                "MONTRNG_NFRMTN_DTIL_IRON TEXT, MONTRNG_NFRMTN_DTIL_HARDNESS TEXT,MONTRNG_NFRMTN_DTIL_TURBIDITY TEXT," +
                "MONTRNG_NFRMTN_DTIL_INFORM TEXT,INFO_ABOUT_WELL_HOUSEHOLD_WELL1 TEXT,INFO_ABOUT_WELL_AFFECTED_FAMILY TEXT,"+
                "MONTRNG_NFRMTN_DTIL_E_COLI TEXT, MONTRNG_NFRMTN_DTIL_FLOURIDE TEXT, MONTRNG_NFRMTN_DTIL_MANGANESE TEXT, " +
                "MONTRNG_NFRMTN_DTIL_TDS TEXT,MONTRNG_NFRMTN_DTIL_TASTE_ODOUR TEXT,"+
                "MONTRNG_NFRMTN_DTIL_HIDDEN TEXT, _URI TEXT PRIMARY KEY)";
        //query create table for well platform picture
        String queryPlatformPicBLB = "create table if not exists WELLASSESSMENT_INFO_ABOUT_WELL_PLATFORM_PICTURE_BLB" +
                "(_TOP_LEVEL_AURI TEXT PRIMARY KEY, VALUE TEXT)";
        String queryPlatformPicBN = "create table if not exists WELLASSESSMENT_INFO_ABOUT_WELL_PLATFORM_PICTURE_BN" +
                "(_TOP_LEVEL_AURI TEXT PRIMARY KEY, CONTENT_TYPE TEXT)";
        //query create table for well picture
        String queryPic = "create table if not exists WELLASSESSMENT_INFO_ABOUT_WELL_WELL_PICTURE_BLB" +
                "(_TOP_LEVEL_AURI TEXT PRIMARY KEY, VALUE TEXT)";
        String queryPic2 = "create table if not exists WELLASSESSMENT_INFO_ABOUT_WELL_WELL_PICTURE_BN" +
                "(_TOP_LEVEL_AURI TEXT PRIMARY KEY, CONTENT_TYPE TEXT)";

        //query create table hardware damaged
        String queryHardwareDamaged = "create table if not exists WELLASSESSMENT_MONITORING_INFORMATION_HARDWAREDAMAGED " +
                "(_URI TEXT PRIMARY KEY,_PARENT_AURI TEXT,VALUE TEXT)";

        //query create table type dirt
        String queryTypeDirt = "create table if not exists WELLASSESSMENT_MONITORING_INFORMATION_TYPEDIRT " +
                "(_URI TEXT PRIMARY KEY,_PARENT_AURI TEXT,VALUE TEXT)";

        //query create table taste1
        String queryTaste1 = "create table if not exists WELLASSESSMENT_MONITORING_INFORMATION_TASTE1 " +
                "(_URI TEXT PRIMARY KEY,_PARENT_AURI TEXT,VALUE TEXT)";

        db.execSQL(query);
        db.execSQL(queryPlatformPicBLB);
        db.execSQL(queryPlatformPicBN);
        db.execSQL(queryPic);
        db.execSQL(queryPic2);
        db.execSQL(queryHardwareDamaged);
        db.execSQL(queryTypeDirt);
        db.execSQL(queryTaste1);

    }
    public void insertPlatformPic(String auri,String blobEncode64){
        SQLiteDatabase db = this.getWritableDatabase();
        //Insert or replace because it can duplicate the _TOP_LEVEL_AURI which make the process stop and can not insert the next new record in the loop
        String sql = "INSERT OR REPLACE into WELLASSESSMENT_INFO_ABOUT_WELL_PLATFORM_PICTURE_BLB "+
                "values("+"'"+auri+"'"+","+"'"+blobEncode64+"'"+")";
        db.execSQL(sql);
        db.close();
    }
    public void insertPic(String auri,String blobEncode64){
        SQLiteDatabase db = this.getWritableDatabase();
        //Insert or replace because it can duplicate the _TOP_LEVEL_AURI which make the process stop and can not insert the next new record in the loop
        String sql = "INSERT OR REPLACE into WELLASSESSMENT_INFO_ABOUT_WELL_WELL_PICTURE_BLB "+
                "values("+"'"+auri+"'"+","+"'"+blobEncode64+"'"+")";
        db.execSQL(sql);
        db.close();
    }
    public void insertHardwareDamaged(String uri, String parent_auri, String value){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "insert into WELLASSESSMENT_MONITORING_INFORMATION_HARDWAREDAMAGED "+
                "values("+"'"+uri+"'"+","+"'"+parent_auri+"'"+","+"'"+value+"'"+")";
        db.execSQL(sql);
        db.close();
    }
    public void insertTypeDirt(String uri, String parent_auri, String value){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "insert into WELLASSESSMENT_MONITORING_INFORMATION_TYPEDIRT "+
                "values("+"'"+uri+"'"+","+"'"+parent_auri+"'"+","+"'"+value+"'"+")";
        db.execSQL(sql);
        db.close();
    }
    public void insertTaste1(String uri, String parent_auri, String value){
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "insert into WELLASSESSMENT_MONITORING_INFORMATION_TASTE1 "+
                "values("+"'"+uri+"'"+","+"'"+parent_auri+"'"+","+"'"+value+"'"+")";
        db.execSQL(sql);
        db.close();
    }
    public void insertRow(String address, String owner, String wellType,String ownerPhone,
                          String wellID, String lat, String lng, String acc, String alt, String lastDate,
                          String recordedDate, String recordingPerson, String recordingOrganization, String recordingOther,
                          String wellIDByFunding, String locationWell, String otherLocation, String fundedByOriginal,
                          String otherFund, String fundedByRehabilitated, String otherFunded1, String householdWell,
                          String dateWell, String dateWell1, String primaryPump, String primaryOther,
                          String diameterOfWell, String diameterOther, String depthDrill, String yield,
                          String sand, String dynamicLevel, String staticLevel, String usedForDrink,
                          String usedForDailyLife, String platform, String wellPlatform, String platformDesign,
                          String drainDesign, String flooded, String height, String wellFlooded,
                          String yearFlooded, String highestFlooded, String waterPoint, String damaged,
                          String damagedOther, String otherDamaged, String environmentCon,
                          String typeDirtOther, String quantityWater, String supplySeasonal, String taste,
                          String otherTaste, String waterQualMonitoring, String arsenic, String ph,
                          String freeClorine, String nitrate, String nitrite, String ion,
                          String hardness, String tubidity, String inform, String householdWell1,
                          String affectedFamily, String e_coli, String flouride, String manganese,
                          String tds, String tasteOdour, String hidden, String _uri
    ){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String insertQuery = "insert into WELLASSESSMENT_CORE(id,ADMNSTRTIVE_INFRMTON_ADDRESS," +
                "INFO_ABOUT_WELL_WELL_OWNER_NAME,INFO_ABOUT_WELL_TYPEOFWELL,INFO_ABOUT_WELL_WELLOWNER_PHONE," +
                "INFO_ABOUT_WELL_WELL_ID,INFO_ABOUT_WELL_WELL_LOCATION_LAT,INFO_ABOUT_WELL_WELL_LOCATION_LNG," +
                "INFO_ABOUT_WELL_WELL_LOCATION_ACC, INFO_ABOUT_WELL_WELL_LOCATION_ALT, "+
                "_LAST_UPDATE_DATE,ADMNSTRTIVE_INFRMTON_RECORDED_DATE,ADMNSTRTIVE_INFRMTON_RECORDING_PERSON," +
                "ADMNSTRTIVE_INFRMTON_RECORDING_ORGANIZATION,ADMNSTRTIVE_INFRMTON_RECORDING_OTHER,INFO_ABOUT_WELL_WELL_ID_BY_FUNDING," +
                "INFO_ABOUT_WELL_LOCATION_WELL,INFO_ABOUT_WELL_OTHERLOCATION,INFO_ABOUT_WELL_FUNDEDBYORIGINAL," +
                "INFO_ABOUT_WELL_OTHERFUNDED ,INFO_ABOUT_WELL_FUNDEDBYREHABILITATED, INFO_ABOUT_WELL_OTHERFUNDED1," +
                "INFO_ABOUT_WELL_HOUSEHOLD_WELL,INFO_ABOUT_WELL_DATE_WELL,INFO_ABOUT_WELL_DATE_WELL1," +
                "INFO_ABOUT_WELL_PRIMARYPUMP, INFO_ABOUT_WELL_PRIMARYOTHER,INFO_ABOUT_WELL_DIAMETER_OF_WELL," +
                "INFO_ABOUT_WELL_DIAMETER_OTHER ,INFO_ABOUT_WELL_DEPTHDRILL, INFO_ABOUT_WELL_YIELD," +
                "INFO_ABOUT_WELL_SAND,INFO_ABOUT_WELL_DYNAMIC,INFO_ABOUT_WELL_STATIC," +
                "INFO_ABOUT_WELL_USEDFORDRINK, INFO_ABOUT_WELL_USEDFORDAILYLIFE, INFO_ABOUT_WELL_PLATFORM," +
                "INFO_ABOUT_WELL_WELLPLATFORM,INFO_ABOUT_WELL_PLATFORMDESIGN, INFO_ABOUT_WELL_DRAINDESIGN," +
                "INFO_ABOUT_WELL_FLOODED, INFO_ABOUT_WELL_HEIGHT, INFO_ABOUT_WELL_WELLFLOODED," +
                "INFO_ABOUT_WELL_YEARFLOODED ,INFO_ABOUT_WELL_HIGHESTFLOODED, MONTORING_INFORMATON_WATERPOINT," +
                "MONTORING_INFORMATON_DAMAGED, MONTORING_INFORMATON_DAMAGEDOTHER, MONTORING_INFORMATON_OTHERDAMAGED," +
                "MONTORING_INFORMATON_ENVIRONMENTCON,MONTORING_INFORMATON_TYPEDIRT_OTHER,MONTORING_INFORMATON_QUANTITYWATER, " +
                "MONTORING_INFORMATON_SUPPLYSEASONAL, MONTORING_INFORMATON_TASTE,MONTORING_INFORMATON_OTHERTASTE, " +
                "MONTRNG_NFRMTN_DTIL_WATERQUAL_MONITORING, MONTRNG_NFRMTN_DTIL_ARSENIC,MONTRNG_NFRMTN_DTIL_PH, " +
                "MONTRNG_NFRMTN_DTIL_FREE_CHLORINE, MONTRNG_NFRMTN_DTIL_NITRATE, MONTRNG_NFRMTN_DTIL_NITRITE, " +
                "MONTRNG_NFRMTN_DTIL_IRON, MONTRNG_NFRMTN_DTIL_HARDNESS, MONTRNG_NFRMTN_DTIL_TURBIDITY, " +
                "MONTRNG_NFRMTN_DTIL_INFORM, INFO_ABOUT_WELL_HOUSEHOLD_WELL1,INFO_ABOUT_WELL_AFFECTED_FAMILY," +
                "MONTRNG_NFRMTN_DTIL_E_COLI, MONTRNG_NFRMTN_DTIL_FLOURIDE, MONTRNG_NFRMTN_DTIL_MANGANESE," +
                "MONTRNG_NFRMTN_DTIL_TDS, MONTRNG_NFRMTN_DTIL_TASTE_ODOUR, "+
                "MONTRNG_NFRMTN_DTIL_HIDDEN, _URI) values((select count(id)+1 from WELLASSESSMENT_CORE),"+
                "'"+address+"'"+","+"'"+owner+"'"+","+"'"+wellType+"'"+","+"'"+ownerPhone+"'"+","+"'"+wellID+"'"+","+
                "'"+lat+"'"+","+"'"+lng+"'"+","+"'"+acc+"'"+","+"'"+alt+"'"+","+"'"+lastDate+"'"+","+"'"+recordedDate+"'"+","+
                "'"+recordingPerson+"'"+","+"'"+recordingOrganization+"'"+","+"'"+recordingOther+"'"+","+"'"+wellIDByFunding+"'"+","+
                "'"+locationWell+"'"+","+"'"+otherLocation+"'"+","+"'"+fundedByOriginal+"'"+","+"'"+otherFund+"'"+","+
                "'"+fundedByRehabilitated+"'"+","+"'"+otherFunded1+"'"+","+"'"+householdWell+"'"+","+"'"+dateWell+"'"+","+
                "'"+dateWell1+"'"+","+"'"+primaryPump+"'"+","+"'"+primaryOther+"'"+","+"'"+diameterOfWell+"'"+","+
                "'"+diameterOther+"'"+","+"'"+depthDrill+"'"+","+"'"+yield+"'"+","+"'"+sand+"'"+","+
                "'"+dynamicLevel+"'"+","+"'"+staticLevel+"'"+","+"'"+usedForDrink+"'"+","+"'"+usedForDailyLife+"'"+","+
                "'"+platform+"'"+","+"'"+wellPlatform+"'"+","+"'"+platformDesign+"'"+","+"'"+drainDesign+"'"+","+
                "'"+flooded+"'"+","+"'"+height+"'"+","+"'"+wellFlooded+"'"+","+"'"+yearFlooded+"'"+","+
                "'"+highestFlooded+"'"+","+"'"+waterPoint+"'"+","+"'"+damaged+"'"+","+"'"+damagedOther+"'"+","+
                "'"+otherDamaged+"'"+","+"'"+environmentCon+"'"+","+"'"+typeDirtOther+"'"+","+"'"+quantityWater+"'"+","+
                "'"+supplySeasonal+"'"+","+"'"+taste+"'"+","+"'"+otherTaste+"'"+","+"'"+waterQualMonitoring+"'"+","+
                "'"+arsenic+"'"+","+"'"+ph+"'"+","+"'"+freeClorine+"'"+","+"'"+nitrate+"'"+","+"'"+nitrite+"'"+","+
                "'"+ion+"'"+","+"'"+hardness+"'"+","+"'"+tubidity+"'"+","+"'"+inform+"'"+","+
                "'"+householdWell1+"'"+","+"'"+affectedFamily+"'"+","+"'"+e_coli+"'"+","+"'"+flouride+"'"+","+
                "'"+manganese+"'"+","+"'"+tds+"'"+","+"'"+tasteOdour+"'"+","+
                "'"+hidden+"'"+","+"'"+_uri+"'"+")";

        sqLiteDatabase.execSQL(insertQuery);
        sqLiteDatabase.close();
    }

    public void deleteUserTbl(){
        String query = "drop table well_user";
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(query);
    }

    public HashMap<String, String> getUser(){
        HashMap<String, String> user = new HashMap<>();
        String query = "select * from well_user";
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(query);
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        if(cursor.moveToFirst()){
            do {
                user.put("username",cursor.getString(1));
                user.put("password",cursor.getString(2));
            }while(cursor.moveToNext());
        }
        return user;
    }

    public float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }
    public ArrayList<WellProperties> getWell_location(Double lat, Double lng, int firstDis, int secondDis){


        ArrayList<WellProperties> well_location_list = new ArrayList<WellProperties>();
        String selectQuery = "select id,ADMNSTRTIVE_INFRMTON_ADDRESS,INFO_ABOUT_WELL_WELL_OWNER_NAME," +
                "INFO_ABOUT_WELL_TYPEOFWELL, _URI, INFO_ABOUT_WELL_WELL_LOCATION_LAT," +
                "INFO_ABOUT_WELL_WELL_LOCATION_LNG from WELLASSESSMENT_CORE";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        String dbLat ="", dbLng = "";
        float dist = 0;
        String village_name[];
        if(cursor.moveToFirst()){
            do{
                dbLat = cursor.getString(5); dbLng = cursor.getString(6);

                if(dbLat.length()>0 && dbLng.length()>0){
                    dist = distFrom(lat,lng,Double.parseDouble(dbLat),Double.parseDouble(dbLng));
                    if(dist > firstDis && dist < secondDis ){
                        //ArrayList<String> map = new ArrayList();
                        village_name = cursor.getString(1).split("-");
                        WellProperties well = new WellProperties();
                        well.setId(cursor.getString(0));
                        well.setAdd(village_name[3].trim());
                        well.setOwner(cursor.getString(2));
                        well.setType(cursor.getString(3));
                        well.setUri(cursor.getString(4));
                        well_location_list.add(well);
                    }
                }


            }while(cursor.moveToNext());
        }
        sqLiteDatabase.close();
        System.out.println(well_location_list.size() + " SIZE");
        return well_location_list;
    }
    public ArrayList<WellProperties> getWell_location_without_gps(){


        ArrayList<WellProperties> well_location_list = new ArrayList<WellProperties>();
        String selectQuery = "select id,ADMNSTRTIVE_INFRMTON_ADDRESS,INFO_ABOUT_WELL_WELL_OWNER_NAME," +
                "INFO_ABOUT_WELL_TYPEOFWELL, _URI, INFO_ABOUT_WELL_WELL_LOCATION_LAT," +
                "INFO_ABOUT_WELL_WELL_LOCATION_LNG from WELLASSESSMENT_CORE";
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        String village_name[];
        if(cursor.moveToFirst()){
            do{
                  //ArrayList<String> map = new ArrayList();
                        village_name = cursor.getString(1).split("-");
                        WellProperties well = new WellProperties();
                        well.setId(cursor.getString(0));
                        well.setAdd(village_name[3].trim());
                        well.setOwner(cursor.getString(2));
                        well.setType(cursor.getString(3));
                        well.setUri(cursor.getString(4));
                        well_location_list.add(well);

            }while(cursor.moveToNext());
        }
        sqLiteDatabase.close();
        System.out.println(well_location_list.size() + " SIZE");
        return well_location_list;
    }
    public Integer selectMaxUserID(){
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectMax = "select id from well_user order by id desc limit 1";
        Cursor cursor = sqLiteDatabase.rawQuery(selectMax, null);
        int lastID = (cursor.moveToFirst() ? cursor.getInt(0) : 0);
        return lastID;

    }
    public ArrayList<String> getAllRows(String _uri){
        ArrayList arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from WELLASSESSMENT_CORE where _URI='"+_uri+"'";
        Cursor cursor = db.rawQuery(sql,null);
        if(cursor.moveToFirst()){
            do{
                arrayList.add(cursor.getString(0));
                arrayList.add(cursor.getString(1));
                arrayList.add(cursor.getString(2));
                arrayList.add(cursor.getString(3));
                arrayList.add(cursor.getString(4));
                arrayList.add(cursor.getString(5));
                arrayList.add(cursor.getString(6));
                arrayList.add(cursor.getString(7));
                arrayList.add(cursor.getString(8));
                arrayList.add(cursor.getString(9));
                arrayList.add(cursor.getString(10));
                arrayList.add(cursor.getString(11));
                arrayList.add(cursor.getString(12));
                arrayList.add(cursor.getString(13));
                arrayList.add(cursor.getString(14));

                arrayList.add(cursor.getString(15));
                arrayList.add(cursor.getString(16));//INFO_ABOUT_WELL_LOCATION_WELL
                arrayList.add(cursor.getString(17));
                arrayList.add(cursor.getString(18));
                arrayList.add(cursor.getString(19));
                arrayList.add(cursor.getString(20));
                arrayList.add(cursor.getString(21));
                arrayList.add(cursor.getString(22));
                arrayList.add(cursor.getString(23));
                arrayList.add(cursor.getString(24));
                arrayList.add(cursor.getString(25));
                arrayList.add(cursor.getString(26));
                arrayList.add(cursor.getString(27));
                arrayList.add(cursor.getString(28));
                arrayList.add(cursor.getString(29));
                arrayList.add(cursor.getString(30));
                arrayList.add(cursor.getString(31));
                arrayList.add(cursor.getString(32));
                arrayList.add(cursor.getString(33));
                arrayList.add(cursor.getString(34));
                arrayList.add(cursor.getString(35));
                arrayList.add(cursor.getString(36));
                arrayList.add(cursor.getString(37));//INFO_ABOUT_WELL_WELLPLATFORM

                arrayList.add(cursor.getString(38));
                arrayList.add(cursor.getString(39));
                arrayList.add(cursor.getString(40));
                arrayList.add(cursor.getString(41));
                arrayList.add(cursor.getString(42));
                arrayList.add(cursor.getString(43));
                arrayList.add(cursor.getString(44));//INFO_ABOUT_WELL_HIGHESTFLOODED

                /* Monitoring */
                arrayList.add(cursor.getString(45));//MONTORING_INFORMATON_WATERPOINT

                arrayList.add(cursor.getString(46));//MONTORING_INFORMATON_DAMAGED
                arrayList.add(cursor.getString(47));//MONTORING_INFORMATON_DAMAGEDOTHER
                arrayList.add(cursor.getString(48));//MONTORING_INFORMATON_OTHERDAMAGED
                arrayList.add(cursor.getString(49));//MONTORING_INFORMATON_ENVIRONMENTCON
                arrayList.add(cursor.getString(50));//MONTORING_INFORMATON_TYPEDIRT_OTHER
                arrayList.add(cursor.getString(51));//MONTORING_INFORMATON_QUANTITYWATER

                arrayList.add(cursor.getString(52));//MONTORING_INFORMATON_SUPPLYSEASONAL
                arrayList.add(cursor.getString(53));//MONTORING_INFORMATON_TASTE
                arrayList.add(cursor.getString(54));//MONTORING_INFORMATON_OTHERTASTE
                arrayList.add(cursor.getString(55));//MONTRNG_NFRMTN_DTIL_WATERQUAL_MONITORING
                arrayList.add(cursor.getString(56));//MONTRNG_NFRMTN_DTIL_ARSENIC
                arrayList.add(cursor.getString(57));//MONTRNG_NFRMTN_DTIL_PH

                arrayList.add(cursor.getString(58));//MONTRNG_NFRMTN_DTIL_FREE_CHLORINE
                arrayList.add(cursor.getString(59));//MONTRNG_NFRMTN_DTIL_NITRATE
                arrayList.add(cursor.getString(60));//MONTRNG_NFRMTN_DTIL_NITRITE
                arrayList.add(cursor.getString(61));//MONTRNG_NFRMTN_DTIL_IRON
                arrayList.add(cursor.getString(62));//MONTRNG_NFRMTN_DTIL_HARDNESS
                arrayList.add(cursor.getString(63));//MONTRNG_NFRMTN_DTIL_TURBIDITY
                arrayList.add(cursor.getString(64));//MONTRNG_NFRMTN_DTIL_INFORM
                arrayList.add(cursor.getString(65));//INFO_ABOUT_WELL_HOUSEHOLD_WELL1
                arrayList.add(cursor.getString(66));//INFO_ABOUT_WELL_AFFECTED_FAMILY

                arrayList.add(cursor.getString(67));//MONTRNG_NFRMTN_DTIL_E_COLI
                arrayList.add(cursor.getString(68));//MONTRNG_NFRMTN_DTIL_FLOURIDE
                arrayList.add(cursor.getString(69));//MONTRNG_NFRMTN_DTIL_MANGANESE
                arrayList.add(cursor.getString(70));//MONTRNG_NFRMTN_DTIL_TDS
                arrayList.add(cursor.getString(71));//MONTRNG_NFRMTN_DTIL_TASTE_ODOUR



            }while (cursor.moveToNext());
        }

        return arrayList;
    }
    public ArrayList<String> getWellPic(String _uri){
        ArrayList<String> arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from WELLASSESSMENT_INFO_ABOUT_WELL_WELL_PICTURE_BLB where _TOP_LEVEL_AURI='"+_uri+"'";
        Cursor cursor = db.rawQuery(sql,null);
        if(cursor.moveToFirst()){
            do{
                arrayList.add(cursor.getString(0));
                arrayList.add(cursor.getString(1));

            }while (cursor.moveToNext());
        }
        return arrayList;
    }
    public ArrayList<String> getWellPlatformPic(String _uri){
        ArrayList<String> arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select * from WELLASSESSMENT_INFO_ABOUT_WELL_PLATFORM_PICTURE_BLB where _TOP_LEVEL_AURI='"+_uri+"'";
        Cursor cursor = db.rawQuery(sql,null);
        if(cursor.moveToFirst()){
           // do{
                arrayList.add(cursor.getString(0));
                arrayList.add(cursor.getString(1));

          //  }while (cursor.moveToNext());
        }
        return arrayList;
    }
    public ArrayList<String> getHardwareDamaged(String _uri){
        ArrayList<String> arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select VALUE from WELLASSESSMENT_MONITORING_INFORMATION_HARDWAREDAMAGED where _PARENT_AURI ='"+_uri+"'";
        Cursor cursor = db.rawQuery(sql,null);
        if(cursor.moveToFirst()){
           do{
            arrayList.add(cursor.getString(0));
           // arrayList.add(cursor.getString(1));
           }while (cursor.moveToNext());
        }
        return arrayList;
    }
    public ArrayList<String> getTypeDirt(String _uri){
        ArrayList<String> arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select VALUE from WELLASSESSMENT_MONITORING_INFORMATION_TYPEDIRT where _PARENT_AURI ='"+_uri+"'";
        Cursor cursor = db.rawQuery(sql,null);
        if(cursor.moveToFirst()){
            do{
                arrayList.add(cursor.getString(0));
                // arrayList.add(cursor.getString(1));
            }while (cursor.moveToNext());
        }
        return arrayList;
    }
    public ArrayList<String> getTaste1(String _uri){
        ArrayList<String> arrayList = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "select VALUE from WELLASSESSMENT_MONITORING_INFORMATION_TASTE1 where _PARENT_AURI ='"+_uri+"'";
        Cursor cursor = db.rawQuery(sql,null);
        if(cursor.moveToFirst()){
            do{
                arrayList.add(cursor.getString(0));
                // arrayList.add(cursor.getString(1));
            }while (cursor.moveToNext());
        }
        return arrayList;
    }

    public String selectMaxDate(){

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String selectMax = "select _LAST_UPDATE_DATE from WELLASSESSMENT_CORE order by id desc limit 1";
        Cursor cursor = sqLiteDatabase.rawQuery(selectMax,null);
        String maxDate = (cursor.moveToFirst() ? cursor.getString(0) : "2013-01-01 00:00:00");
        return maxDate;

    }
    public int dbSyncCountPlatformPic(){
        int count = 0;
        String selectQuery = "SELECT  * FROM WELLASSESSMENT_INFO_ABOUT_WELL_PLATFORM_PICTURE_BLB";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        count = cursor.getCount();
        database.close();
        return count;
    }
    public int dbSyncCount(){
        int count = 0;
        String selectQuery = "SELECT  * FROM WELLASSESSMENT_CORE";
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null);
        count = cursor.getCount();
        database.close();
        return count;
    }
    public void insertUser(String id, String username, String password, String org){
        String query = "insert into well_user values("+"'"+id+"'"+","+"'"+username+"'"+","+
                "'"+password+"'"+","+"'"+org+"'"+")";
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(query);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }




}
