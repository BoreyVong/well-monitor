package data.bridge;

import java.io.Serializable;

/**
 * Created by yinseng on 7/31/14.
 * is a clase that store value, but we can get the value only the first
 */
public class ODKFirstOnly implements Serializable {

    //private static final long serialVersionUID = 12L;
    private boolean isFirstTimeOnly ;
    private String value;

    public ODKFirstOnly(){
        isFirstTimeOnly = true;
        value = "";
    }

    public ODKFirstOnly(boolean isFirstTimeOnly, byte[] fidValue) {
        this.isFirstTimeOnly = isFirstTimeOnly;
    }

    public ODKFirstOnly(boolean isFirstTimeOnly, String value) {
        this.isFirstTimeOnly = isFirstTimeOnly ;
        this.value = value;
    }

    public boolean isFirstTimeOnly() {
        return isFirstTimeOnly;
    }

    public void setFirstTimeOnly(boolean isFirstTimeOnly) {
        this.isFirstTimeOnly = isFirstTimeOnly;
    }

    public String getValue() {
        isFirstTimeOnly = true;
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
