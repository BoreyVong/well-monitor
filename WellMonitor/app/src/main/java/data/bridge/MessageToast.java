package data.bridge;

import android.content.Context;

import com.github.johnpersano.supertoasts.SuperToast;

/**
 * Created by yinseng on 7/23/14.
 */
public class MessageToast {
    private Context context;

    public MessageToast(Context context){
        this.context = context;
    }
    public void MessageWarning(String message){
        SuperToast superToast = new SuperToast(context);
        superToast.setText(message);
        superToast.setBackground(SuperToast.Background.ORANGE);
        superToast.setAnimations(SuperToast.Animations.FADE);
        superToast.setIcon(SuperToast.Icon.Light.EDIT, SuperToast.IconPosition.LEFT);
        superToast.show();
    }

    public void MessageError(String message){
        SuperToast superToast = new SuperToast(context);
        superToast.setText(message);
        superToast.setBackground(SuperToast.Background.RED);
        superToast.setAnimations(SuperToast.Animations.FADE);
        superToast.setIcon(SuperToast.Icon.Light.EXIT, SuperToast.IconPosition.LEFT);
        superToast.show();
    }

    public void MessageInfo(String message){
        SuperToast superToast = new SuperToast(context);
        superToast.setText(message);
        superToast.setBackground(SuperToast.Background.BLUE);
        superToast.setAnimations(SuperToast.Animations.FADE);
        superToast.setDuration(SuperToast.Duration.SHORT);
        superToast.setIcon(SuperToast.Icon.Light.INFO, SuperToast.IconPosition.LEFT);
        superToast.show();
    }

    public void MessageSuccess(String message){
        SuperToast superToast = new SuperToast(context);
        superToast.setText(message);
        superToast.setBackground(SuperToast.Background.GREEN);
        superToast.setAnimations(SuperToast.Animations.FADE);
        superToast.setIcon(SuperToast.Icon.Light.SAVE, SuperToast.IconPosition.LEFT);
        superToast.show();
    }

}
