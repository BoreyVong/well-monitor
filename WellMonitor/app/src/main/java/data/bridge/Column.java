package data.bridge;

/**
 * Created by Borey on 4/10/15.
 * All columns in the table
 */

public class Column {

    /* column string represent */
    public static String ADDRESS;
    public static String WELLOWNERNAME;
    public static String TYPEOFWELL;
    public static String WELLOWNERPHONE;
    public static String WELLID;
    public static String LOCATIONLAT;
    public static String LOCATIONLNG;
    public static String LOCATIONACC;
    public static String LOCATIONALT;
    public static String LASTUPDATEDATE;
    public static String RECORDEDDATE;
    public static String RECORDINGPERSON;

    public static String RECORDINGORGANIZATION;
    public static String RECORDINGOTHER;
    //public static String WELLPICTURE;
    public static String WELLIDBYFUNDING;
    public static String LOCATIONWELL;
    public static String OTHERLOCATION;
    public static String FUNDEDBYORIGINAL;
    public static String OTHERFUND;
    public static String FUNDEDBYREHABILITATED;
    public static String OTHERFUNDED1;

    public static String HOUSEHOLDWELL;
    public static String DATEWELL;
    public static String DATEWELL1;
    public static String PRIMARYPUMP;
    public static String PRIMARYOTHER;
    public static String DIAMETEROFWELL;
    public static String DIAMETEROTHER;
    public static String DEPTHDRILL;
    public static String YIELD;
    public static String SAND;


    public static String DYNAMICLEVEL;
    public static String STATICLEVEL;
    public static String USEDFORDRINK;
    public static String USEDFORDAILYLIFE;
    public static String PLATFORM;
    public static String PLATFORMPICTURE;
    public static String WELLPLATFORM;
    public static String PLATFORMDESIGN;
    public static String DRAINDESIGN;
    public static String FLOODED;

    public static String HEIGHT;
    public static String WELLFLOODED;
    public static String YEARFLOODED;
    public static String HIGHESTFLOODED;
    public static String HIDDEN;
    public static String _URI;

    public static String PLATFORMPICTURETYPE;
    public static String _TOP_LEVEL_AURI_PLAT;

    public static String PICTURE;
    public static String _TOP_LEVEL_AURI_PIC;

    public static String HOUSEHOLDWELL1;
    public static String AFFECTEDFAMILY;

    /*Monitoring column */

    public static String WATERPOINT;
    public static String DAMAGED;
    public static String DAMAGEDOTHER;
    //Create culumn for hardware damaged
    public static String _H_URI;
    public static String _H_PARENT_AURI;
    public static String _H_VALUE;
    public static String OTHERDAMAGED;
    public static String ENVIRONMENTCON;
    public static String TYPEDIRTOTHER;
    public static String QUANTITYWATER;
    public static String SUPPLYSEASONAL;
    public static String TASTE;
    public static String OTHERTASTE;
    public static String WATERQUALMONITORING;
    public static String ARSENIC;
    public static String PH;
    public static String FREECLORINE;
    public static String NITRATE;
    public static String NITRITE;
    public static String IRON;
    public static String HARDNESS;
    public static String TUBIDITY;
    public static String E_COLI;
    public static String FLOURIDE;
    public static String MANGANESE;
    public static String TDS;
    public static String TASTEODOUR;

    public static String INFORM;
    //Type Dirt
    public static String _TD_URI;
    public static String _TD_PARENT_AURI;;
    public static String _TD_VALUE;
    //Taste1
    public static String _TA_URI;
    public static String _TA_PARENT_AURI;;
    public static String _TA_VALUE;


    /** end **/

    /* column in real table */




}
