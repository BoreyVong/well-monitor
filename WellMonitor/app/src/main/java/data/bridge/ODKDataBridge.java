package data.bridge;

import java.io.Serializable;

/**
 * Created by yignseng on 7/31/14.
 */
public  class ODKDataBridge implements Serializable {

    public  ODKFirstOnly getRecordedDate() {
        return recordedDate;
    }

    public  void setRecordedDate(ODKFirstOnly recordedDate) {
        this.recordedDate = recordedDate;
    }

    public  void setRecordingPerson(ODKFirstOnly recordingPerson) {
        this.recordingPerson = recordingPerson;
    }

    public  ODKFirstOnly getRecordingOrganization() {
        return recordingOrganization;
    }

    public  void setRecordingOrganization(ODKFirstOnly recordingOrganization) {
        this.recordingOrganization = recordingOrganization;
    }

    public  ODKFirstOnly getRecordingOther() {
        return recordingOther;
    }

    public  void setRecordingOther(ODKFirstOnly recordingOther) {
        this.recordingOther = recordingOther;
    }

    public  ODKFirstOnly getProvince() {
        return province;
    }

    public  void setProvince(ODKFirstOnly province) {
        this.province = province;
    }

    public  ODKFirstOnly getCommune() {
        return commune;
    }

    public  void setCommune(ODKFirstOnly commune) {
        this.commune = commune;
    }

    public  ODKFirstOnly getVillage() {
        return village;
    }

    public  void setVillage(ODKFirstOnly village) {
        this.village = village;
    }

    public  ODKFirstOnly getVillageCode() {
        return villageCode;
    }

    public  void setVillageCode(ODKFirstOnly villageCode) {
        this.villageCode = villageCode;
    }

    public  ODKFirstOnly getLocationLat() {
        return locationLat;
    }

    public  void setLocationLat(ODKFirstOnly locationLat) {
        this.locationLat = locationLat;
    }

    public  ODKFirstOnly getLocationLng() {
        return locationLng;
    }

    public  void setLocationLng(ODKFirstOnly locationLng) {
        this.locationLng = locationLng;
    }

    public  ODKFirstOnly getWellPicture() {
        return wellPicture;
    }

    public  void setWellPicture(ODKFirstOnly wellPicture) {
        this.wellPicture = wellPicture;
    }

    public  ODKFirstOnly getWellID() {
        return wellID;
    }

    public  void setWellID(ODKFirstOnly wellID) {
        this.wellID = wellID;
    }

    public  ODKFirstOnly getWellIDByFunding() {
        return wellIDByFunding;
    }

    public  void setWellIDByFunding(ODKFirstOnly wellIDByFunding) {
        this.wellIDByFunding = wellIDByFunding;
    }

    public  ODKFirstOnly getLocationWell() {
        return locationWell;
    }

    public  void setLocationWell(ODKFirstOnly locationWell) {
        this.locationWell = locationWell;
    }

    public  ODKFirstOnly getOtherLocation() {
        return otherLocation;
    }

    public  void setOtherLocation(ODKFirstOnly otherLocation) {
        this.otherLocation = otherLocation;
    }

    public  ODKFirstOnly getWellOwnerName() {
        return wellOwnerName;
    }

    public  void setWellOwnerName(ODKFirstOnly wellOwnerName) {
        this.wellOwnerName = wellOwnerName;
    }

    public  ODKFirstOnly getWellOwnerPhone() {
        return wellOwnerPhone;
    }

    public  void setWellOwnerPhone(ODKFirstOnly wellOwnerPhone) {
        this.wellOwnerPhone = wellOwnerPhone;
    }

    public  ODKFirstOnly getFundedByOriginal() {
        return fundedByOriginal;
    }

    public  void setFundedByOriginal(ODKFirstOnly fundedByOriginal) {
        this.fundedByOriginal = fundedByOriginal;
    }

    public  ODKFirstOnly getOtherFunded() {
        return otherFunded;
    }

    public  void setOtherFunded(ODKFirstOnly otherFund) {
        this.otherFunded = otherFund;
    }

    public  ODKFirstOnly getFundedByRehabilitated() {
        return fundedByRehabilitated;
    }

    public  void setFundedByRehabilitated(ODKFirstOnly fundedByRehabilitated) {
        this.fundedByRehabilitated = fundedByRehabilitated;
    }

    public  ODKFirstOnly getOtherFunded1() {
        return otherFunded1;
    }

    public  void setOtherFunded1(ODKFirstOnly otherFunded1) {
        this.otherFunded1 = otherFunded1;
    }

    public  ODKFirstOnly getHouseholdWell() {
        return householdWell;
    }

    public  void setHouseholdWell(ODKFirstOnly householdWell) {
        this.householdWell = householdWell;
    }

    public  ODKFirstOnly getTypeOfWell() {
        return typeOfWell;
    }

    public  void setTypeOfWell(ODKFirstOnly typeOfWell) {
        this.typeOfWell = typeOfWell;
    }

    public  ODKFirstOnly getDateWell() {
        return dateWell;
    }

    public  void setDateWell(ODKFirstOnly dateWell) {
        this.dateWell = dateWell;
    }

    public  ODKFirstOnly getDateWell1() {
        return dateWell1;
    }

    public  void setDateWell1(ODKFirstOnly dateWell1) {
        this.dateWell1 = dateWell1;
    }

    public  ODKFirstOnly getPrimaryPump() {
        return primaryPump;
    }

    public  void setPrimaryPump(ODKFirstOnly primaryPump) {
        this.primaryPump = primaryPump;
    }

    public  ODKFirstOnly getPrimaryOther() {
        return primaryOther;
    }

    public  void setPrimaryOther(ODKFirstOnly primaryOther) {
        this.primaryOther = primaryOther;
    }

    public  ODKFirstOnly getDiameterOfWell() {
        return diameterOfWell;
    }

    public  void setDiameterOfWell(ODKFirstOnly diameterOfWell) {
        this.diameterOfWell = diameterOfWell;
    }

    public  ODKFirstOnly getDiameterOther() {
        return diameterOther;
    }

    public  void setDiameterOther(ODKFirstOnly diameterOther) {
        this.diameterOther = diameterOther;
    }

    public  ODKFirstOnly getDepthDrill() {
        return depthDrill;
    }

    public  void setDepthDrill(ODKFirstOnly depthDrill) {
        this.depthDrill = depthDrill;
    }

    public  ODKFirstOnly getYield() {
        return yield;
    }

    public  void setYield(ODKFirstOnly yield) {
        this.yield = yield;
    }

    public  ODKFirstOnly getSand() {
        return sand;
    }

    public  void setSand(ODKFirstOnly sand) {
        this.sand = sand;
    }

    public  ODKFirstOnly getDynamicLevel() {
        return dynamicLevel;
    }

    public  void setDynamicLevel(ODKFirstOnly dynamicLevel) {
        this.dynamicLevel = dynamicLevel;
    }

    public  ODKFirstOnly getStaticLevel() {
        return staticLevel;
    }

    public  void setStaticLevel(ODKFirstOnly Level) {
        this.staticLevel = Level;
    }

    public  ODKFirstOnly getUsedForDrink() {
        return usedForDrink;
    }

    public  void setUsedForDrink(ODKFirstOnly usedForDrink) {
        this.usedForDrink = usedForDrink;
    }

    public  ODKFirstOnly getUsedForDailyLife() {
        return usedForDailyLife;
    }

    public  void setUsedForDailyLife(ODKFirstOnly usedForDailyLife) {
        this.usedForDailyLife = usedForDailyLife;
    }

    public  ODKFirstOnly getPlatform() {
        return platform;
    }

    public  void setPlatform(ODKFirstOnly platform) {
        this.platform = platform;
    }

    public  ODKFirstOnly getPlatformPicture() {
        return platformPicture;
    }

    public  void setPlatformPicture(ODKFirstOnly platformPicture) {
        this.platformPicture = platformPicture;
    }

    public  ODKFirstOnly getWellPlatform() {
        return wellPlatform;
    }

    public  void setWellPlatform(ODKFirstOnly wellPlatform) {
        this.wellPlatform = wellPlatform;
    }

    public  ODKFirstOnly getPlatformDesign() {
        return platformDesign;
    }

    public  void setPlatformDesign(ODKFirstOnly platformDesign) {
        this.platformDesign = platformDesign;
    }

    public  ODKFirstOnly getDrainDesign() {
        return drainDesign;
    }

    public  void setDrainDesign(ODKFirstOnly drainDesign) {
        this.drainDesign = drainDesign;
    }

    public  ODKFirstOnly getFlooded() {
        return flooded;
    }

    public  void setFlooded(ODKFirstOnly flooded) {
        this.flooded = flooded;
    }

    public  ODKFirstOnly getHeight() {
        return height;
    }

    public  void setHeight(ODKFirstOnly height) {
        this.height = height;
    }

    public  ODKFirstOnly getWellFlooded() {
        return wellFlooded;
    }

    public  void setWellFlooded(ODKFirstOnly wellFlooded) {
        this.wellFlooded = wellFlooded;
    }

    public  ODKFirstOnly getYearFlooded() {
        return yearFlooded;
    }

    public  void setYearFlooded(ODKFirstOnly yearFlooded) {
       this.yearFlooded = yearFlooded;
    }

    public  ODKFirstOnly getHighestFlooded() {
        return highestFlooded;
    }

    public  void setHighestFlooded(ODKFirstOnly highestFlooded) {
        this.highestFlooded = highestFlooded;
    }
    public ODKFirstOnly getRecordingPerson(){
        return recordingPerson;
    }
    public ODKFirstOnly getAddress() {
        return address;
    }

    public void setAddress(ODKFirstOnly address) {
        this.address = address;
    }
    public ODKFirstOnly getWellLocation() {
        return wellLocation;
    }

    public void setWellLocation(ODKFirstOnly wellLocation) {
        this.wellLocation = wellLocation;
    }

    private static final long serialVersionUID = 11L;
        private  ODKFirstOnly recordedDate;
        public  ODKFirstOnly recordingPerson;
        public  ODKFirstOnly recordingOrganization;   //*1.3 Recording Organization, select one option
        public  ODKFirstOnly recordingOther;
        public  ODKFirstOnly province;                //Address: province
        public  ODKFirstOnly commune;                 //Address: commune
        public  ODKFirstOnly village;                 //Address: village
        public  ODKFirstOnly villageCode;             //Village Code
        public  ODKFirstOnly locationLat;
        public  ODKFirstOnly locationLng;
        public  ODKFirstOnly wellPicture;             //**2.2 Picture of the well, image
        public  ODKFirstOnly wellID;
        public  ODKFirstOnly wellIDByFunding;
        public  ODKFirstOnly locationWell;            //*2.4 Location of Well, select one option
        public  ODKFirstOnly otherLocation;           // 2.4
        public  ODKFirstOnly wellOwnerName;           //2.5 Well Owner Manager

        public  ODKFirstOnly wellOwnerPhone;
        public  ODKFirstOnly fundedByOriginal;        //*2.7 Funded By (original well)
        public  ODKFirstOnly otherFunded;               //Other funded: Please Specify
        public  ODKFirstOnly fundedByRehabilitated;   //*2.8 Funded By (rehabilitated well)
        public  ODKFirstOnly otherFunded1;            //Other Funded By (rehabilitated well : Please Specify
        public  ODKFirstOnly householdWell;           //2.91 How many household use the well?
        public  ODKFirstOnly householdWell1;          //2.92 How many household use the well?
        public  ODKFirstOnly numberFamilyAffected;    //2.92 How many household use the well?
        public  ODKFirstOnly typeOfWell;              //*2.10 Type of Well, select option type, Pump Well, Combinded Well, Unprotected Well.
        public  ODKFirstOnly dateWell;                // 2.11 Date the Well was constructed
        public  ODKFirstOnly dateWell1;               //2.11.1 Latest date the well was rehabilitated
        public  ODKFirstOnly primaryPump;             //*2.12 Pump types, Select one option
        public  ODKFirstOnly primaryOther;            //other pump: Please Specify
        public  ODKFirstOnly diameterOfWell;          //*2.13 Diameter of Well Tube (mm), Select one option
        public  ODKFirstOnly diameterOther;           // Other diameter: Please Specify
        public  ODKFirstOnly depthDrill;              //*2.14 The Depth of Well, select one option
        public  ODKFirstOnly yield;                   //*2.15 How many yield of water in meter cube per hour?, select one option
        public  ODKFirstOnly sand;                    //*2.16 Is the well filling with sand?, select one option
        public  ODKFirstOnly dynamicLevel;            //2.17 Dynamic Level
        public  ODKFirstOnly staticLevel;                //2.18  Level

        public  ODKFirstOnly usedForDrink;            //*2.19 Is this well used for Drinking and Cooking?, select one option
        public  ODKFirstOnly usedForDailyLife;        //*2.20 Is this well used for daily activities (laundry, bathing, crop-watering)?, select one option
        public  ODKFirstOnly platform;                //*2.21 Does the Well have a platform?
        public  ODKFirstOnly platformPicture;         //**2.21.1 Picture of the Platform, image
        public  ODKFirstOnly wellPlatform;            //*2.21.2 Is the platform broken?, select one option
        public  ODKFirstOnly platformDesign;          //*2.21.3 Was the platform designed according to the national standard?, select one option
        public  ODKFirstOnly drainDesign;             //*2.21.4 Was the outflow drain designed according to the national standard?, select one option
        public  ODKFirstOnly flooded;                 //*2.21.5 Does the platform get flooded?, select one option
        public  ODKFirstOnly height;                  //*2.21.6 What is the height of the platform (from the land  next to it)?, select one option
        public  ODKFirstOnly wellFlooded;             //*2.22 Does the well get flooded?, select one option
        public  ODKFirstOnly yearFlooded;             //*2.22.1 As far as you remember, which year did the well get highest flooded?, select one option
        public  ODKFirstOnly highestFlooded;          //*2.22.2 In the highest flood that you remember, what was the highest level the water reached near the well? (in relation to land level near the well), select one option

        public ODKFirstOnly address;
        public ODKFirstOnly wellLocation;

        /* Monitoring field */
        public ODKFirstOnly waterPoint;
        public ODKFirstOnly damaged;
        public ODKFirstOnly damagedOther;
        public ODKFirstOnly hardwareDamaged;
        public ODKFirstOnly otherDamaged;
        public ODKFirstOnly environmentCon;
        public ODKFirstOnly typeDirt;
        public ODKFirstOnly typeDirtOther;
        public ODKFirstOnly quantityWater;
        public ODKFirstOnly supplySeasonal;
        public ODKFirstOnly taste;
        public ODKFirstOnly taste1;
        public ODKFirstOnly otherTaste;
        public ODKFirstOnly waterQualMonitoring;
        public ODKFirstOnly arsenic;
        public ODKFirstOnly ph;
        public ODKFirstOnly freeChlorine;
        public ODKFirstOnly nitrate;
        public ODKFirstOnly nitrite;
        public ODKFirstOnly iron;
        public ODKFirstOnly hardness;
        public ODKFirstOnly turbidity;
        public ODKFirstOnly e_coli;
        public ODKFirstOnly flouride;
        public ODKFirstOnly manganese;
        public ODKFirstOnly tds;
        public ODKFirstOnly tasteOdour;
        public ODKFirstOnly inform;

        public ODKFirstOnly hidden;

    public ODKFirstOnly getHidden() {
        return hidden;
    }

    public void setHidden(ODKFirstOnly hidden) {
        this.hidden = hidden;
    }

    public ODKFirstOnly getE_coli() {
        return e_coli;
    }

    public void setE_coli(ODKFirstOnly e_coli) {
        this.e_coli = e_coli;
    }

    public ODKFirstOnly getFlouride() {
        return flouride;
    }

    public void setFlouride(ODKFirstOnly flouride) {
        this.flouride = flouride;
    }

    public ODKFirstOnly getManganese() {
        return manganese;
    }

    public void setManganese(ODKFirstOnly manganese) {
        this.manganese = manganese;
    }

    public ODKFirstOnly getTds() {
        return tds;
    }

    public void setTds(ODKFirstOnly tds) {
        this.tds = tds;
    }

    public ODKFirstOnly getTasteOdour() {
        return tasteOdour;
    }

    public void setTasteOdour(ODKFirstOnly tasteOdour) {
        this.tasteOdour = tasteOdour;
    }

    public ODKFirstOnly getHouseholdWell1() {
        return householdWell1;
    }

    public ODKFirstOnly getNumberFamilyAffected() {
        return numberFamilyAffected;
    }

    public void setNumberFamilyAffected(ODKFirstOnly numberFamilyAffected) {
        this.numberFamilyAffected = numberFamilyAffected;
    }

    public void setHouseholdWell1(ODKFirstOnly householdWell1) {
        this.householdWell1 = householdWell1;
    }
    public ODKFirstOnly getInform() {
        return inform;
    }

    public void setInform(ODKFirstOnly inform) {
        this.inform = inform;
    }

    public ODKFirstOnly getTurbidity() {
        return turbidity;
    }

    public void setTurbidity(ODKFirstOnly turbidity) {
        this.turbidity = turbidity;
    }

    public ODKFirstOnly getHardness() {
        return hardness;
    }

    public void setHardness(ODKFirstOnly hardness) {
        this.hardness = hardness;
    }

    public ODKFirstOnly getIron() {
        return iron;
    }

    public void setIron(ODKFirstOnly iron) {
        this.iron = iron;
    }

    public ODKFirstOnly getNitrite() {
        return nitrite;
    }

    public void setNitrite(ODKFirstOnly nitrite) {
        this.nitrite = nitrite;
    }

    public ODKFirstOnly getNitrate() {
        return nitrate;
    }

    public void setNitrate(ODKFirstOnly nitrate) {
        this.nitrate = nitrate;
    }

    public ODKFirstOnly getFreeChlorine() {
        return freeChlorine;
    }

    public void setFreeChlorine(ODKFirstOnly freeChlorine) {
        this.freeChlorine = freeChlorine;
    }

    public ODKFirstOnly getPh() {
        return ph;
    }

    public void setPh(ODKFirstOnly ph) {
        this.ph = ph;
    }

    public ODKFirstOnly getArsenic() {
        return arsenic;
    }

    public void setArsenic(ODKFirstOnly arsenic) {
        this.arsenic = arsenic;
    }

    public ODKFirstOnly getWaterQualMonitoring() {
        return waterQualMonitoring;
    }

    public void setWaterQualMonitoring(ODKFirstOnly waterQualMonitoring) {
        this.waterQualMonitoring = waterQualMonitoring;
    }

    public ODKFirstOnly getOtherTaste() {
        return otherTaste;
    }

    public void setOtherTaste(ODKFirstOnly otherTaste) {
        this.otherTaste = otherTaste;
    }

    public ODKFirstOnly getTaste1() {
        return taste1;
    }

    public void setTaste1(ODKFirstOnly taste1) {
        this.taste1 = taste1;
    }

    public ODKFirstOnly getTaste() {
        return taste;
    }

    public void setTaste(ODKFirstOnly taste) {
        this.taste = taste;
    }

    public ODKFirstOnly getSupplySeasonal() {
        return supplySeasonal;
    }

    public void setSupplySeasonal(ODKFirstOnly supplySeasonal) {
        this.supplySeasonal = supplySeasonal;
    }

    public ODKFirstOnly getQuantityWater() {
        return quantityWater;
    }

    public void setQuantityWater(ODKFirstOnly quantityWater) {
        this.quantityWater = quantityWater;
    }

    public ODKFirstOnly getTypeDirtOther() {
        return typeDirtOther;
    }

    public void setTypeDirtOther(ODKFirstOnly typeDirtOther) {
        this.typeDirtOther = typeDirtOther;
    }

    public ODKFirstOnly getTypeDirt() {
        return typeDirt;
    }

    public void setTypeDirt(ODKFirstOnly typeDirt) {
        this.typeDirt = typeDirt;
    }

    public ODKFirstOnly getEnvironmentCon() {
        return environmentCon;
    }

    public void setEnvironmentCon(ODKFirstOnly environmentCon) {
        this.environmentCon = environmentCon;
    }

    public ODKFirstOnly getOtherDamaged() {

        return otherDamaged;
    }

    public void setOtherDamaged(ODKFirstOnly otherDamaged) {
        this.otherDamaged = otherDamaged;
    }

    public ODKFirstOnly getHardwareDamaged() {
        return hardwareDamaged;
    }

    public void setHardwareDamaged(ODKFirstOnly hardwareDamaged) {
        this.hardwareDamaged = hardwareDamaged;
    }

    public ODKFirstOnly getWaterPoint() {
        return waterPoint;
    }

    public void setWaterPoint(ODKFirstOnly waterPoint) {
        this.waterPoint = waterPoint;
    }

    public ODKFirstOnly getDamaged() {
        return damaged;
    }

    public void setDamaged(ODKFirstOnly damaged) {
        this.damaged = damaged;
    }

    public ODKFirstOnly getDamagedOther() {
        return damagedOther;
    }

    public void setDamagedOther(ODKFirstOnly damagedOther) {
        this.damagedOther = damagedOther;
    }
        /* public ODKDataBridge(ODKFirstOnly recordedDate, ODKFirstOnly recordingPerson, ODKFirstOnly recordingOrganization,
                         ODKFirstOnly recordingOther, ODKFirstOnly villageCode, ODKFirstOnly wellOwnerPhone, ODKFirstOnly fundedByOriginal, ODKFirstOnly fundedByRehabilitated,
                         ODKFirstOnly otherFunded1, ODKFirstOnly householdWell, ODKFirstOnly typeOfWell, ODKFirstOnly dateWell, ODKFirstOnly dateWell1,
                         ODKFirstOnly primaryPump, ODKFirstOnly primaryOther, ODKFirstOnly diameterOfWell, ODKFirstOnly diameterOther, ODKFirstOnly depthDrill,
                         ODKFirstOnly yield, ODKFirstOnly sand, ODKFirstOnly dynamicLevel, ODKFirstOnly Level, ODKFirstOnly usedForDrink,
                         ODKFirstOnly usedForDailyLife, ODKFirstOnly platform, ODKFirstOnly platformPicture, ODKFirstOnly wellPlatform, ODKFirstOnly platformDesign,
                         ODKFirstOnly drainDesign, ODKFirstOnly flooded, ODKFirstOnly height, ODKFirstOnly wellFlooded, ODKFirstOnly yearFlooded,
                         ODKFirstOnly highestFlooded) {

    }*/

    public ODKDataBridge() {
    }
}