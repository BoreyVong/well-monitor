package data.bridge;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;

import org.odk.collect.wellmonitor.activities.FormEntryActivity;
import org.odk.collect.wellmonitor.application.Collect;
import org.odk.collect.wellmonitor.provider.FormsProviderAPI;
import org.odk.collect.wellmonitor.widgets.QuestionWidget;

import open.org.kh.wellmonitor.R;

/**
 * Created by yinseng on 7/30/14.
 */
public class ODKBridge {

    private transient Context context;
    private MessageToast messageToast;
    private AsyncResponse asyncResponse;
    private int round;

    public ODKBridge(Context context){
        this.context = context;
    }


    public void loadFormByID(String id){
        String sortOrder = FormsProviderAPI.FormsColumns.DISPLAY_NAME + " ASC, " + FormsProviderAPI.FormsColumns.JR_VERSION + " DESC";
        Cursor c = context.getContentResolver().query(FormsProviderAPI.FormsColumns.CONTENT_URI, null, null, null, sortOrder);
        if (c.getCount()>0){
            long idFormsTable = 0 ;
            c.moveToFirst();
            do{
                String formName = c.getString(c.getColumnIndex(FormsProviderAPI.FormsColumns.JR_FORM_ID));
                if(formName.equals(id)){
                    idFormsTable = c.getLong(c.getColumnIndex("_id"));
                    break;
                }
            }while (c.moveToNext());


            Uri formUri = ContentUris.withAppendedId(FormsProviderAPI.FormsColumns.CONTENT_URI, idFormsTable);
            Collect.getInstance().getActivityLogger().logAction(this, "onListItemClick", formUri.toString());

       /* // caller wants to view/edit a form, so launch formentryactivity

        Intent intent=new Intent(Intent.ACTION_EDIT, formUri);*/
            Intent intent=new Intent(context, FormEntryActivity.class);
            intent.setData(formUri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }else{
            messageToast=new MessageToast(context);
            messageToast.MessageError(Collect.getInstance().getResources().getString(R.string.msg_noforms));
        }


    }
    public void loadFormByID(String id,ODKDataBridge odkDataBridge){
        String sortOrder = FormsProviderAPI.FormsColumns.DISPLAY_NAME + " ASC, " + FormsProviderAPI.FormsColumns.JR_VERSION + " DESC";
        Cursor c = context.getContentResolver().query(FormsProviderAPI.FormsColumns.CONTENT_URI, null, null, null, sortOrder);
        if (c.getCount()>0){
            long idFormsTable = 0 ;
            c.moveToFirst();
            do{
                String formName = c.getString(c.getColumnIndex(FormsProviderAPI.FormsColumns.JR_FORM_ID));
                if(formName.equals(id)){
                    idFormsTable = c.getLong(c.getColumnIndex("_id"));
                    break;
                }
            }while (c.moveToNext());


            Uri formUri = ContentUris.withAppendedId(FormsProviderAPI.FormsColumns.CONTENT_URI, idFormsTable);
            Collect.getInstance().getActivityLogger().logAction(this, "onListItemClick", formUri.toString());

            Intent intent=new Intent(context, FormEntryActivity.class);
            intent.setData(formUri);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("ODKDataBridge", odkDataBridge);
            intent.putExtra("ViewPatient", (java.io.Serializable) asyncResponse);

            try {
                context.startActivity(intent);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else{
            messageToast=new MessageToast(context);
            messageToast.MessageError(Collect.getInstance().getResources().getString(R.string.msg_noforms));
        }

    }
    public static String getFieldID(String refFieldString) {

        String fieldID="";
        if (null != refFieldString && refFieldString.length() > 0) {
            String[] mySplit= refFieldString.split("/");
            fieldID= mySplit[mySplit.length-1];
        }
        return fieldID;
    }

    public ODKDataBridge setDefaultValue(QuestionWidget questionWidget,ODKDataBridge odkDataBridge) {
        String s3 = questionWidget.getPrompt().getFormElement().getBind().getReference().toString();
        String s4 = getFieldID(s3);

        if (s4.equalsIgnoreCase("recording_person")) {
            if (!odkDataBridge.getRecordingPerson().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getRecordingPerson().getValue());
        }
        else if (s4.equalsIgnoreCase("recording_organization")) {
            if (!odkDataBridge.getRecordingOrganization().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getRecordingOrganization().getValue());
        }
        else if(s4.equalsIgnoreCase("recording_other")){
            if (!odkDataBridge.getRecordingOther().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getRecordingOther().getValue());
        }
        else if(s4.equalsIgnoreCase("address")){
            if (!odkDataBridge.getAddress().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getAddress().getValue());
        }
        else if(s4.equalsIgnoreCase("village_code")){
            if (!odkDataBridge.getVillageCode().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getVillageCode().getValue());
        }
        else if(s4.equalsIgnoreCase("well_location")){
            if (!odkDataBridge.getWellLocation().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellLocation().getValue());
        }
        else if(s4.equalsIgnoreCase("well_picture")){
            if (!odkDataBridge.getWellPicture().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellPicture().getValue());
        }
        else if(s4.equalsIgnoreCase("well_id")){
            if (!odkDataBridge.getWellID().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellID().getValue());
        }
        else if(s4.equalsIgnoreCase("well_id_by_funding")){
            if (!odkDataBridge.getWellIDByFunding().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellIDByFunding().getValue());
        }
        else if(s4.equalsIgnoreCase("location_well")){
            if (!odkDataBridge.getLocationWell().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getLocationWell().getValue());
        }
        else if(s4.equalsIgnoreCase("otherlocation")){
            if (!odkDataBridge.getOtherLocation().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getOtherLocation().getValue());
        }
        else if(s4.equalsIgnoreCase("well_owner_name")){
            if (!odkDataBridge.getWellOwnerName().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellOwnerName().getValue());
        }
        else if(s4.equalsIgnoreCase("wellowner_phone")){
            if (!odkDataBridge.getWellOwnerPhone().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellOwnerPhone().getValue());
        }
        else if(s4.equalsIgnoreCase("fundedbyoriginal")){
            if (!odkDataBridge.getFundedByOriginal().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getFundedByOriginal().getValue());
        }
        else if(s4.equalsIgnoreCase("otherfunded")){
            if (!odkDataBridge.getOtherFunded().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getOtherFunded().getValue());
        }
        else if(s4.equalsIgnoreCase("fundedbyrehabilitated")){
            if (!odkDataBridge.getFundedByRehabilitated().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getFundedByRehabilitated().getValue());
        }
        else if(s4.equalsIgnoreCase("otherfunded1")){
            if (!odkDataBridge.getOtherFunded1().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getOtherFunded1().getValue());
        }
        else if(s4.equalsIgnoreCase("household_well")){
            if (!odkDataBridge.getHouseholdWell().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getHouseholdWell().getValue());
        }
        else if(s4.equalsIgnoreCase("household_well1")){
            if (!odkDataBridge.getHouseholdWell1().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getHouseholdWell1().getValue());
        }
        else if(s4.equalsIgnoreCase("affected_family")){
            if (!odkDataBridge.getNumberFamilyAffected().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getHouseholdWell1().getValue());
        }
        else if(s4.equalsIgnoreCase("typeofwell")){
            if (!odkDataBridge.getTypeOfWell().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getTypeOfWell().getValue());
        }
        else if(s4.equalsIgnoreCase("date_well")){
            if (!odkDataBridge.getDateWell().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getDateWell().getValue());
        }
        else if(s4.equalsIgnoreCase("date_well1")){
            if (!odkDataBridge.getDateWell1().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getDateWell1().getValue());
        }
        else if(s4.equalsIgnoreCase("primarypump")){
            if (!odkDataBridge.getPrimaryPump().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getPrimaryPump().getValue());
        }
        else if(s4.equalsIgnoreCase("primaryother")){
            if (!odkDataBridge.getPrimaryOther().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getPrimaryOther().getValue());
        }
        else if(s4.equalsIgnoreCase("diameter_of_well")){
            if (!odkDataBridge.getDiameterOfWell().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getDiameterOfWell().getValue());
        }
        else if(s4.equalsIgnoreCase("diameter_other")){
            if (!odkDataBridge.getDiameterOther().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getDiameterOther().getValue());
        }
        else if(s4.equalsIgnoreCase("depthdrill")){
            if (!odkDataBridge.getDepthDrill().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getDepthDrill().getValue());
        }
        else if(s4.equalsIgnoreCase("yield")){
            if (!odkDataBridge.getYield().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getYield().getValue());
        }
        else if(s4.equalsIgnoreCase("sand")){
            if (!odkDataBridge.getSand().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getSand().getValue());
        }
        else if(s4.equalsIgnoreCase("dynamic")){
            if (!odkDataBridge.getDynamicLevel().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getDynamicLevel().getValue());
        }
        else if(s4.equalsIgnoreCase("static")){
            if (!odkDataBridge.getStaticLevel().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getStaticLevel().getValue());
        }
        else if(s4.equalsIgnoreCase("usedfordrink")){
            if (!odkDataBridge.getUsedForDrink().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getUsedForDrink().getValue());
        }
        else if(s4.equalsIgnoreCase("usedfordailylife")){
            if (!odkDataBridge.getUsedForDailyLife().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getUsedForDailyLife().getValue());
        }
        else if(s4.equalsIgnoreCase("platform")){
            if (!odkDataBridge.getPlatform().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getPlatform().getValue());
        }
        else if(s4.equalsIgnoreCase("platform_picture")){
            if (!odkDataBridge.getPlatformPicture().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getPlatformPicture().getValue());
        }
        else if(s4.equalsIgnoreCase("wellplatform")){
            if (!odkDataBridge.getWellPlatform().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellPlatform().getValue());
        }
        else if(s4.equalsIgnoreCase("platformdesign")){
            if (!odkDataBridge.getPlatformDesign().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getPlatformDesign().getValue());
        }
        else if(s4.equalsIgnoreCase("draindesign")){
            if (!odkDataBridge.getDrainDesign().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getDrainDesign().getValue());
        }
        else if(s4.equalsIgnoreCase("flooded")){
            if (!odkDataBridge.getFlooded().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getFlooded().getValue());
        }
        else if(s4.equalsIgnoreCase("height")){
            if (!odkDataBridge.getHeight().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getHeight().getValue());
        }
        else if(s4.equalsIgnoreCase("wellflooded")){
            if (!odkDataBridge.getWellFlooded().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getWellFlooded().getValue());
        }
        else if(s4.equalsIgnoreCase("yearflooded")){
            if (!odkDataBridge.getYearFlooded().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getYearFlooded().getValue());
        }
        else if(s4.equalsIgnoreCase("highestflooded")){
            if (!odkDataBridge.getHighestFlooded().isFirstTimeOnly())
                questionWidget.setDefaultValue(odkDataBridge.getHighestFlooded().getValue());
        }
        else if (s4.equalsIgnoreCase("waterpoint")){
            if (!odkDataBridge.getWaterPoint().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getWaterPoint().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("damaged")){
            if (!odkDataBridge.getDamaged().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getDamaged().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("damagedother")){
            if (!odkDataBridge.getDamagedOther().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getDamagedOther().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("hardwaredamaged")){
            if (!odkDataBridge.getHardwareDamaged().isFirstTimeOnly()) {
                questionWidget.setDefaultValue(odkDataBridge.getHardwareDamaged().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("otherdamaged")){
            if (!odkDataBridge.getOtherDamaged().isFirstTimeOnly()) {
                questionWidget.setDefaultValue(odkDataBridge.getOtherDamaged().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("environmentcon")){
            if (!odkDataBridge.getEnvironmentCon().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getEnvironmentCon().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("typedirt")){
            if (!odkDataBridge.getTypeDirt().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getTypeDirt().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("typedirt_other")){
            if (!odkDataBridge.getTypeDirtOther().isFirstTimeOnly()) {
                questionWidget.setDefaultValue(odkDataBridge.getTypeDirtOther().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("quantitywater")){
            if (!odkDataBridge.getQuantityWater().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getQuantityWater().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("supplyseasonal")){
            if (!odkDataBridge.getSupplySeasonal().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getSupplySeasonal().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("taste")){
            if (!odkDataBridge.getTaste().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getTaste().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("taste1")){
            if (!odkDataBridge.getTaste1().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getTaste1().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("othertaste")){
            if (!odkDataBridge.getOtherTaste().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getOtherTaste().getValue());
            }
        } else if (s4.equalsIgnoreCase("waterqual_monitoring")){
            if (!odkDataBridge.getWaterQualMonitoring().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getWaterQualMonitoring().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("arsenic")){
            if (!odkDataBridge.getArsenic().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getArsenic().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("ph")){
            if (!odkDataBridge.getPh().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getPh().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("free_chlorine")){
            if (!odkDataBridge.getFreeChlorine().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getFreeChlorine().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("nitrate")){
            if (!odkDataBridge.getNitrate().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getNitrate().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("nitrite")){
            if (!odkDataBridge.getNitrite().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getNitrite().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("iron")){
            if (!odkDataBridge.getIron().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getIron().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("hardness")){
            if (!odkDataBridge.getHardness().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getHardness().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("turbidity")){
            if (!odkDataBridge.getTurbidity().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getTurbidity().getValue());
            }
        }

        else if (s4.equalsIgnoreCase("e_coli")){
            if (!odkDataBridge.getE_coli().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getE_coli().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("flouride")){
            if (!odkDataBridge.getFlouride().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getFlouride().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("manganese")){
            if (!odkDataBridge.getManganese().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getManganese().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("tds")){
            if (!odkDataBridge.getTds().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getTds().getValue());
            }
        }
        else if (s4.equalsIgnoreCase("taste_odour")){
            if (!odkDataBridge.getTasteOdour().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getTasteOdour().getValue());
            }
        }

        else if (s4.equalsIgnoreCase("inform")){
            if (!odkDataBridge.getInform().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getInform().getValue());
            }
        }

        else if (s4.equalsIgnoreCase("hidden")){
            if (!odkDataBridge.getHidden().isFirstTimeOnly()){
                questionWidget.setDefaultValue(odkDataBridge.getHidden().getValue());
            }
        }


        return odkDataBridge;

    }



}
