package data.bridge;

/**
 * Created by yinseng on 7/2/14.
 */
public interface AsyncResponse {
    void publicProgressBridge(String... objs);
}
